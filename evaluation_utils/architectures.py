import tensorflow as tf
import numpy as np
import codat.nn.unet
import codat.nn.deep_labv3 as deepnet
import glob
import wkw
import tqdm
from collections import defaultdict
import scipy.io as sio
import shutil
import attr
from skimage import transform
from scipy import ndimage
import os

class Architectures:
    def __init__(self,type=1,res=1,ckpt=None,DATA = "/",predict_size=[624,624,142],reg=None):
        self.feature_maps = None
        # kernel siz contractive path
        self.kernel_size = []
        # kernel siz expansive path
        self.kernel_size_exp = []
        self.input_shape = []
        self.num_classes = 3
        self.additional_args = {}
        self.data_format = 'channels_last'
        self.res = res
        self.resolution = '/2-2-1/' if self.res == 2 else '/1/'
        self.true_size = np.array(predict_size)
        self.big_cube_size = np.array(predict_size) if self.res == 1 else np.array(predict_size)//[2,2,1] 
        self.pool_size = None
        self.act_out = None
        self.ckpt_file = ckpt
        self.type = type
        self.kernel_regularizer = None
        self.reg = reg
        self.add_seg = 2 if 'seg_1_' in self.ckpt_file else 1
        self.load_architecture()
        self.SEG_PATH = None #DATA+"segmentation"+'/2-2-1/'
        self.RAW_DATA_PATH = DATA+"color"+'/2-2-1/'
        self.raw_data = wkw.Dataset.open(self.RAW_DATA_PATH)
        self.TARGET_DATA_PATH = ("/tmpscratch/webknossos/Connectomics_Department/"
                                 + "2018-11-13_scMS109_1to7199_v01_mag2_varun_myelin_training_annotations"
                                 + "/segmentation/1/")
        
        self.target_annotations = wkw.Dataset.open(self.TARGET_DATA_PATH)
        self.model = None

        
    def predict(self,coor,input_shape=None,preload=True,wkw_out=None,thresh_inside=0.5,thresh_myelin=0.5,folders=None):
        coor = coor//[2,2,1] if self.res == 2 else coor
        if self.model is None:
            self.initiate_model()
        if input_shape is None:
            input_shape = self.input_shape
        add_seg = True if 'seg_1_' in self.ckpt_file else False
        if wkw_out is None:
            pred = self.model.predict_wkw(self.RAW_DATA_PATH,coor, self.big_cube_size, wkw_out=None, 
                            input_size = input_shape, f_pre=self.preprocess(), add_border=True,seg=self.add_seg,
                            wkw_seg=self.SEG_PATH,preload=preload)
            pred_ex = np.exp(pred)
            pred_soft = pred_ex/np.expand_dims(np.sum(pred_ex,axis=3),3)
            return pred_soft
        else:    
            post_write = {'func':self.write_wkw,
                          'thresh_myelin':thresh_myelin,
                          'thresh_inside':thresh_inside,
                         'folders':folders}
            return self.model.predict_wkw(self.RAW_DATA_PATH,coor, self.big_cube_size, 
                                          wkw_out=wkw_out, input_size = input_shape,
                                          f_pre=self.preprocess(),post_write=post_write,
                                          add_border=True,seg=add_seg,
                                          wkw_seg=self.SEG_PATH,preload=preload)
    
    def read_tc(self,coor):
        tc = self.target_annotations.read(coor,self.true_size)[0]
        tc = transform.resize(tc,self.big_cube_size,
                             anti_aliasing=False,order=0,
                             preserve_range=True).astype(np.int32)
        return tc
    
    def write_wkw(self,coor,arrays,base_dir,folders=['segmentation'],delete_prev=False):
        assert len(folders) == len(arrays),"mismatched input and output sets Class Architectures"
        dsets = {}
        deletion = False
        coor = coor//[2,2,1] if self.res == 2 else coor
        for i in folders:
            dir_ = base_dir+i+self.resolution
            if not os.path.exists(dir_):
                os.makedirs(dir_)
            if delete_prev:
                shutil.rmtree(dir_,ignore_errors=True)
                print("deleted exisiting folder ",dir_)
                dsets[i] = wkw.Dataset.create(dir_,  wkw.Header(np.uint8))
                deletion = True
            else :
                dsets[i] = wkw.Dataset.open(dir_)
        
        for i,arr in zip(folders,arrays):
            print("writing data to ",dsets[i].root)
            dsets[i].write(coor,arr.astype(np.uint8))
            print("written at coordinates ",coor)
        return deletion

    
    def load_architecture(self):

        if self.type == 1:
            self.feature_maps = [16, 32, 64, 128]
            self.kernel_size = [[[3, 3, 3], [3, 3, 3]], [[3, 3, 3], [3, 3, 3]],
                           [[3, 3, 3], [3, 3, 1]], [[3, 3, 3], [3, 3, 1]]]
            self.kernel_size_exp = [[[3, 3, 3], [3, 3, 3]], [[3, 3, 3], [3, 3, 3]],
                               [[3, 3, 3], [3, 3, 3]], [[3, 3, 3], [3, 3, 3]]]
            self.input_shape = (1,364, 364, 88, self.add_seg)
            self.pool_size = None
            self.act_out = None
        elif self.type == 2:
            self.feature_maps = [8, 16, 32, 64, 128, 128]
            # kernel siz contractive path
            self.kernel_size = [[[3, 3, 3], [3, 3, 1]],[[3, 3, 3], [3, 3, 1]], [[3, 3, 3], [3, 3, 3]],
                           [[3, 3, 3], [3, 3, 3]], [[3, 3, 3], [3, 3, 3]],[[3, 3, 3], [3, 3, 3]]]
            # kernel siz expansive path
            self.kernel_size_exp = [[[3, 3, 3], [3, 3, 1]],[[3, 3, 3], [3, 3, 1]], [[3, 3, 3], [3, 3, 1]],
                               [[3, 3, 3], [3, 3, 1]], [[3, 3, 3], [3, 3, 3]]]
            self.pool_size = [[2, 2, 1],[2, 2, 1],[2, 2, 2],[2,2,2],[2, 2, 2]]
            self.act_out = None
            self.input_shape = (1,444, 444, 104,self.add_seg)
        elif self.type == 3:
            self.feature_maps = [8, 16, 32, 64, 128]
            # kernel siz contractive path
            self.kernel_size = [[[3, 3, 3], [3, 3, 3]],[[3, 3, 3], [3, 3, 3]], [[3, 3, 3], [3, 3, 1]],
                           [[3, 3, 3], [3, 3, 1]], [[3, 3, 3], [3, 3, 1]]]
            # kernel siz expansive path
            self.kernel_size_exp = [[[3, 3, 3], [3, 3, 3]],[[3, 3, 3], [3, 3, 3]], [[3, 3, 3], [3, 3, 1]],
                               [[3, 3, 3], [3, 3, 1]], [[3, 3, 3], [3, 3, 1]]]
            self.pool_size = [[2, 2, 1],[2, 2, 2],[2, 2, 2],[2, 2, 1]]
            self.act_out = None
            self.input_shape = (1,256, 256, 104,self.add_seg)
        elif self.type == 4:
            self.feature_maps = [16, 32, 64, 128]

            self.additional_args['aspp_feature_maps'] = [128,64,64,64,64,128]
            self.additional_args['atrous_rates'] = [6,12,18]
            self.additional_args['kernel_size_atrous'] = [[3,3,3],[3,3,3],[3,3,3]]
            # kernel sizes in format (xyz)
            # kernel siz contractive path
            self.kernel_size = [[[3, 3, 3], [3, 3, 3]], [[3, 3, 3], [3, 3, 3]],
                           [[3, 3, 3], [3, 3, 1]], [[3, 3, 3], [3, 3, 1]]]
            # kernel siz expansive path
            self.kernel_size_exp = [[[3, 3, 3], [3, 3, 3]], [[3, 3, 3], [3, 3, 3]],
                               [[3, 3, 3], [3, 3, 3]], [[3, 3, 3], [3, 3, 3]]]
            self.pool_size = [[2, 2, 1],[2, 2, 1],[2, 2, 2],[2, 2, 2]]
            self.act_out = None
            self.input_shape = (1,364, 364, 88,self.add_seg)#(1,596, 596, 136,self.add_seg)
            # if (self.reg is not None):
#                 self.kernel_regularizer = tf.contrib.layers.l2_regularizer(self.reg)

        elif self.type == 5:
            self.feature_maps = [16, 32, 64, 128]

            self.additional_args['aspp_feature_maps'] = [128,32,32,32,32,32,128]
            self.additional_args['atrous_rates'] = [4,8,12,16]
            self.additional_args['kernel_size_atrous'] = [[3,3,3],[3,3,3],[3,3,3],[3,3,3]]
            # kernel sizes in format (xyz)
            # kernel siz contractive path
            self.kernel_size = [[[3, 3, 3], [3, 3, 3]], [[3, 3, 3], [3, 3, 3]],
                           [[3, 3, 3], [3, 3, 3]], [[3, 3, 3], [3, 3, 3]]]
            # kernel siz expansive path
            self.kernel_size_exp = [[[3, 3, 3], [3, 3, 3]], [[3, 3, 3], [3, 3, 3]],
                               [[3, 3, 3], [3, 3, 3]], [[3, 3, 3], [3, 3, 3]]]
            self.pool_size = [[2, 2, 1],[2, 2, 1],[2, 2, 2],[2, 2, 2]]
            self.act_out = None
            self.input_shape = (1,364, 364, 88,self.add_seg)
        elif self.type == 6:
            self.feature_maps = [8, 16, 32, 64]
            ## downtrodden feature maps to remove texture composiiton
            self.additional_args['aspp_feature_maps']  = [32,16,16,16,16,64]
            self.additional_args['atrous_rates'] = [6,12,18]
            self.additional_args['kernel_size_atrous'] = [[3,3,3],[3,3,3],[3,3,3]]
            # kernel sizes in format (xyz)
            # kernel siz contractive path
            self.kernel_size = [[[3, 3, 3], [3, 3, 3]], [[3, 3, 3], [3, 3, 3]],
                           [[3, 3, 3], [3, 3, 1]], [[3, 3, 3], [3, 3, 1]]]
            # kernel siz expansive path
            self.kernel_size_exp = [[[3, 3, 3], [3, 3, 3]], [[3, 3, 3], [3, 3, 3]],
                               [[3, 3, 3], [3, 3, 3]], [[3, 3, 3], [3, 3, 3]]]
            self.pool_size = [[2, 2, 1],[2, 2, 1],[2, 2, 2],[2, 2, 2]]
            self.act_out = None
            self.input_shape = (1,364, 364, 88,self.add_seg)
            
    def preprocess(self):
        seg_pre = True if 'seg_normalization' in self.ckpt_file else False
        if seg_pre:
            print("using distance transform for cell mask")
        def preproc_raw(raw):
            mean = 143.0835021480915
            std = 47.77946098242964 * 0.99
            print(self.add_seg)
            if self.add_seg == 2:
                data = (np.asarray(raw[:,:,:,0], dtype=np.float32) - mean) / std
                if seg_pre:
                    seg = ndimage.distance_transform_edt(1-raw[:,:,:,1])
                    seg = (seg-4)/2.5
                else :
                    seg = raw[...,1]

                return np.stack((data,raw[:,:,:,1]),axis=-1)
            else :
                data = (raw- mean) / std
                if data.shape[0]==1:
                    data = data[0,:,:,:,np.newaxis]
                return data
        # dim = 3 if self.data_format == 'channels_last' else 0

        return preproc_raw
    
    def initiate_model(self,ckpt=None):
        tf.reset_default_graph()
        if self.type in [4,5,6]:
            net = deepnet.DeepLab(self.feature_maps,
                          self.additional_args['aspp_feature_maps'],
                          self.additional_args['atrous_rates'],
                          kernel_size=self.kernel_size,
                          kernel_size_exp=self.kernel_size_exp,
                          kernel_regularizer = self.kernel_regularizer,
                          kernel_size_atrous=self.additional_args['kernel_size_atrous'],
                          pool_size=self.pool_size,
                          num_classes=self.num_classes,
                          data_format=self.data_format)
        else :
            net = codat.nn.unet.UNet(self.feature_maps,
                          kernel_size=self.kernel_size,
                          kernel_size_exp=self.kernel_size_exp,
                          pool_size=self.pool_size,
                          num_classes=self.num_classes,
                          data_format=self.data_format)
        net.build_model(self.input_shape)
        sess = net._get_session()
        if ckpt is not None:
            self.ckpt_file = ckpt
        else:
            assert self.ckpt_file is not None, "empty checkpoint file Class Architectures"
        net.load_from_ckpt(self.ckpt_file)
        self.model = net
        return net