import numpy as np

from sklearn.metrics import precision_recall_curve,average_precision_score
import tqdm
from collections import defaultdict
import scipy.io as sio
import attr


@attr.s
class PredHolder(object):
    target = attr.ib()
    prediction = attr.ib()
    ckpt = attr.ib(default='')

@attr.s
class PredHolder(object):
    target = attr.ib()
    prediction = attr.ib()
    ckpt = attr.ib(default='')


class Evaluate:
    
    @attr.s
    class ResultHolder(object):
        pr_curve = attr.ib(factory=list)
        precision_score = attr.ib(factory=list)
        f1_scores = attr.ib(factory=list)
        thresholds = attr.ib(factory=list)
        
    def __init__(self,num_classes,model_evals,avg_precision=False,f1_score=True):
        self.num_classes = num_classes
        self.model_results = defaultdict(Evaluate.ResultHolder) 
        self.avg_precision = avg_precision
        self.f1_score = f1_score
        self.model_eval(model_evals)

        
    def print_results(self):
        for i,val in self.model_results.items():
            print(i+'\n')
            print(val.f1_scores,val.precision_score,val.thresholds)
            
    def model_eval(self,evals):
        results = defaultdict(list)
        for i,val in tqdm.tqdm(evals.items(),total=len(evals)):
            res = self.PR_curve(self.num_classes,val)
            self.model_results[i].pr_curve.append(res['curves'])
            self.model_results[i].precision_score.append(res['precision_score'])
            self.model_results[i].f1_scores.append(res['f1_scores'])
            self.model_results[i].thresholds.append(res['thresholds'])
            print(i)
            if self.f1_score:
                print("myelin f1 score: {} \t inside f1_score: {}".format(*res['f1_scores']))
                print("myelin precision score: {} \t inside precision score: {}".format(
                    res['curves'][0][0][res['thresholds'][0]],
                    res['curves'][1][0][res['thresholds'][1]]
                ))
                print("myelin recall score: {} \t inside recall score: {}".format(
                    res['curves'][0][1][res['thresholds'][0]],
                    res['curves'][1][1][res['thresholds'][1]]
                ))
                print("myelin threshold: {} \t inside threshold: {}".format(
                    res['curves'][0][2][res['thresholds'][0]],
                    res['curves'][1][2][res['thresholds'][1]]
                ))
            if self.avg_precision:
                print("myelin avg precision: {}\t inside avg precision: {}".format(*res['precision_score']))

            
    def PR_curve(self,num_classes,val):
        tc_con = np.concatenate([i.target.ravel() for i  in val],axis=0)
        results = defaultdict(list)
        for classes in range(1,num_classes):
            tc_con_1 = (tc_con == classes)+0
            soft_con = np.concatenate([i.prediction[:,:,:,classes].ravel() for i  in val],axis=0)
            if self.f1_score:
                pr = precision_recall_curve(tc_con_1, soft_con)
                mask = pr[0]+pr[1] != 0
                value1 = pr[0][mask]
                value2 = pr[1][mask]
                f1 = 2*value1*value2/(value1+value2)
                results['curves'].append(pr)
                results['f1_scores'].append(np.max(f1))
                results['thresholds'].append(np.argmax(f1))
            if self.avg_precision:
                results['precision_score'].append(average_precision_score(tc_con_1, soft_con))

        return results
