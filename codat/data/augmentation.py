"""

Module for 3D EM data augmentation.

Created on 17.02.19 19:53

@author: Benedkt Staffler <benedikt.staffler@brain.mpg.de>

Warping is from elektroNN3:
https://github.com/ELEKTRONN/elektronn3/blob/master/elektronn3/data/coord_transforms.py

"""

import numpy as np
from scipy.stats import beta
import itertools


def cutout(raw, cutout_size, val=0, c_dim=3, num_regions=1):
    """
    
    Perform input data augmentation by cutout.
    TODO: add reference
    
    Args:
        raw: 4d nd.array
            Raw input data
        cutout_size: tuple of int or float
            Size of the cutout box in each spatial dimension specified as
            an absolute size using integers >= 1 or as a relative size if < 1.
        val: float
            The value that is used in the cutout region.
        c_dim: int
            The channel dimension of the input. Can be 0 or 3.
        num_regions: int
            The number of regions that are cut out.

    Returns:
        raw: 4d nd.array
            The input data with a random cutout of the specified size set to 0.
    """

    raw_shape = raw.shape
    if c_dim == 0:
        spatial_shape = raw_shape[1:]
    else:
        spatial_shape = raw_shape[:-1]

    cutout_size = np.asarray(cutout_size)
    if np.all(cutout_size < 1) and np.all(cutout_size > 0):
        cutout_size = np.round(spatial_shape * cutout_size)
    elif not np.all(cutout_size >= 1) or \
        not all(np.round(cutout_size) == cutout_size):
        raise RuntimeError('Cutout shape must in (0, 1) or an integer.')
    cutout_size = np.asarray(cutout_size, dtype=np.int32)

    idx = np.zeros((3, ), dtype=np.int32)
    for _ in range(num_regions):
        idx[0] = np.random.randint(0, spatial_shape[0] - cutout_size[0] + 1)
        idx[1] = np.random.randint(0, spatial_shape[1] - cutout_size[1] + 1)
        idx[2] = np.random.randint(0, spatial_shape[2] - cutout_size[2] + 1)

        if c_dim == 0:
            raw[:, idx[0]:idx[0] + cutout_size[0],
                   idx[1]:idx[1] + cutout_size[1],
                   idx[2]:idx[2] + cutout_size[2]] = val
        else:
            raw[idx[0]:idx[0] + cutout_size[0],
                idx[1]:idx[1] + cutout_size[1],
                idx[2]:idx[2] + cutout_size[2], :] = val
    return raw


def input_mixup(x1, y1, x2, y2, alpha=0.25):
    """
    Input mixup.
    TODO: add reference

    Args:
        x1: 4d nd.array
        y1: 4d nd array
        x2: 4d nd.array
        y2: 4d nd array

    Returns:
        x: 4d nd array
            The mixed input.
        y: 4d nd array
            The mixed target values.
    """

    l = beta.rvs(alpha, alpha)
    x = l * x1 + (1 - l) * x2
    y = l * np.asarray(y1, dtype=np.float32) + \
        (1 - l) * np.asarray(y2, dtype=np.float32)
    return x, y, l


def translate(dz, dy, dx):
    return np.array([
        [1.0, 0.0, 0.0, dz],
        [0.0, 1.0, 0.0, dy],
        [0.0, 0.0, 1.0, dx],
        [0.0, 0.0, 0.0, 1.0]
    ], dtype=np.float)


def make_dest_coords(sh):
    """
    Make coordinate list for destination array of shape sh
    """
    xx, yy, zz = np.mgrid[0:sh[0], 0:sh[1], 0:sh[2]]
    hh = np.ones(sh, dtype=np.int)
    coords = np.concatenate([xx[..., None], yy[..., None],
                             zz[..., None], hh[..., None]], axis=-1)
    return coords.astype(np.float)


def make_dest_corners(sh):
    """
    Make coordinate list of the corners of destination array of shape sh
    """
    corners = list(itertools.product(*([0, 1],) * 3))
    sh = np.subtract(sh, 1)  # 0-based indices
    corners = np.multiply(sh, corners)
    corners = np.hstack((corners, np.ones((8, 1))))  # homogeneous coords
    return corners


def get_random_warpmat(lock_z=False, perspective=False, amount=1.0, red_z=1.,
                       spatial_order='xyz'):
    """
    
    Args:
        lock_z: logical
            If True, no spatial transformation in the z-direction is performed.
        perspective: logical
            Enable projection transformation.
        amount: float
            Controls the strenght of the deformation.
        red_z: float
            The non-diagonal terms of the spatial transformation are multiplied
            by this number.
        spatial_order: string
            Ordering of spatial dimensions ('zxy', 'xyz').
    Returns:
        W: 4x4 matrix
            Warping matrix with respect to homogeneous coordinates (x, y, z, 1)
            (for spatial_order xyz and analogously for zxy)
            The top-left 3x3 submatrix of W corresponds to spatial
            transformations.
            W[:3, 3] corresponds to translation in x, y, z respectively.
            W[3, :3] corresponds to a projective projections as it typically
            requires to normalize divide the last component of the coordinate
            vector to one again.

    """
    warpmat = np.eye(4, dtype=float)
    amount *= 0.1
    perturb = np.random.uniform(-amount, amount, (4, 4))
    perturb[:, 3] = 0  # no translation

    if red_z is None:
        red_z = 1.

    if lock_z:
        if spatial_order == 'zxy':
            perturb[0] = 0
            perturb[:, 0] = 0
            perturb[0, :3] *= red_z
            perturb[:3, 0] *= red_z
        else: #xyz
            perturb[2] = 0
            perturb[:, 2] = 0
            perturb[2, :3] *= red_z
            perturb[:3, 2] *= red_z

    if not perspective:
        perturb[3] = 0
    else:
        perturb[3, :3] *= 0.05  # perspective parameters need to be very small
        np.clip(perturb[3, :3], -3e-3, 3e-3, out=perturb[3, :3])

    warpmat = warpmat + perturb
    return warpmat


def map_coordinates_nearest(src, coords, lo):
    u = np.int32(np.round(coords[..., 0] - lo[0]))
    v = np.int32(np.round(coords[..., 1] - lo[1]))
    w = np.int32(np.round(coords[..., 2] - lo[2]))
    return src[u,v,w]


def map_coordinates_linear(src, coords, lo):
    u = coords[..., 0] - lo[0]
    v = coords[..., 1] - lo[1]
    w = coords[..., 2] - lo[2]
    u0 = np.int32(u)
    u1 = u0 + 1
    du = u - u0
    v0 = np.int32(v)
    v1 = v0 + 1
    dv = v - v0
    w0 = np.int32(w)
    w1 = w0 + 1
    dw = w - w0
    val = src[u0, v0, w0] * (1 - du) * (1 - dv) * (1 - dw) + \
          src[u1, v0, w0] * du * (1 - dv) * (1 - dw) + \
          src[u0, v1, w0] * (1 - du) * dv * (1 - dw) + \
          src[u0, v0, w1] * (1 - du) * (1 - dv) * dw + \
          src[u1, v0, w1] * du * (1 - dv) * dw + \
          src[u0, v1, w1] * (1 - du) * dv * dw + \
          src[u1, v1, w0] * du * dv * (1 - dw) + \
          src[u1, v1, w1] * du * dv * dw
    return val


def warp_patch(patch_shape, inp_src, target_src, patch_out_shape=None,
               lock_z=True, amount=1, target_discrete_idx=None, red_z=1.,
               perspective=False, data_format='channels_last',
               warp_mat=None, lo=None):
    """
    Cutout a warped patch from inp_src and target src.
    
    Args:
        patch_shape: list
            Spatial dimension of the input patch that is extracted from inp_src.
        inp_src: 4d numpy.ndarray
            The raw data input cube.
        target_src: 4d numpy.ndarray
            The target data cube.
        patch_out_shape: list
            Spatial dimension of the output patch that is extracted from
            target_src. If None, then target_shape will be set to patch_shape.
        lock_z: logical
            If true, then no shearing along z-dimension is done.
        amount: float
            Controls the strenght of the warping.
        target_discrete_idx: list[int]
            Specifies the channels of target_src that are treated as discrete.
            None treats all channels as discrete.
        red_z: float
            Reduction factor for warping in z-direction.
        data_format: string
            'channels_first' or 'channels_last'.
        perspective: logical
            Do a perspective transformation as well (corresponds to the entries
            M[3, :3] in the homogeneous projection matrix).
        warp_mat: 4x4 numpy.ndarray
            The warping matrix (for debugging).
        lo: numpy.array
            Offsets for cube (for debugging).

    Returns:
        inp_patch: 4d numpy.ndarray
            Warped input data of size patch_shape.
        target_patch: 4d numpy.ndarray
            Corresponding target cube of shape patch_out_shape centerd in
            inp_patch.
        lo: numpy.array
            Offsets of current cube (for debugging).
    """

    if warp_mat is None:
        warp_mat = get_random_warpmat(lock_z=lock_z, amount=amount,
                                      perspective=perspective, red_z=red_z)
    M_inv = np.linalg.inv(warp_mat.astype(np.float64))

    num_channels = inp_src.shape[3]
    if data_format == 'channels_first':
        num_channels = inp_src.shape[0]
        inp_src = np.moveaxis(inp_src, 0, -1)
        target_src = np.moveaxis(target_src, 0, -1)
    input_shape = inp_src.shape[:3]
    target_shape = target_src.shape[:3]
    patch_shape = tuple(patch_shape)

    if patch_out_shape is None:
        patch_out_shape = patch_shape
    else:
        patch_out_shape = tuple(patch_out_shape)

    b = np.asarray(np.subtract(input_shape, target_shape) / 2, dtype=np.uint32)
    border = np.subtract(patch_shape, patch_out_shape) / 2

    # computing a box with the patch dimensions and deforming it by multiplying
    # it by the M_inv
    dest_corners = make_dest_corners(patch_shape)
    src_corners = np.dot(M_inv, dest_corners.T).T
    dest_corners = make_dest_corners(patch_out_shape)
    target_corners = np.dot(M_inv, dest_corners.T).T

    # checking that the range = (max value - min value) in each axis of the
    # deformed box is not greater than the target_shape
    max_diff = lambda a: np.amax(a, axis=0) - np.amin(a, axis=0)
    rangos = max_diff(src_corners[:, :3]) + 1
    rangos_t = max_diff(target_corners[:, :3]) + 1
    if np.any(rangos_t > np.asarray(target_shape)) or \
            np.any(rangos > np.asarray(input_shape)):
        return None, None, None

    if lo is None:
        lo = np.asarray([np.random.randint(0, (abs(target_shape[i] -
                                                   np.floor(rangos_t[i])) - 1))
                         for i in range(len(rangos_t))])

    # calculate the dense coordinate transformations
    dest_coords = make_dest_coords(patch_shape)
    src_coords_inp = np.tensordot(dest_coords, M_inv, axes=[[-1], [1]])
    if np.any(warp_mat[3, :3] != 0):
        # homogeneous divide (only necessary for perspective transformation)
        src_coords_inp /= src_coords_inp[..., 3][..., None]
    src_coords_inp = src_coords_inp[..., :3]

    if np.any(border > 0):
        dest_coords = make_dest_coords(patch_out_shape)
        src_coords_target = np.tensordot(dest_coords, M_inv, axes=[[-1], [1]])
        if np.any(warp_mat[3, :3] != 0):
            # homogeneous divide (only necessary for perspective transformation)
            src_coords_target /= src_coords_target[..., 3][..., None]
        src_coords_target = src_coords_target[..., :3]
    else:
        src_coords_target = src_coords_inp

    min_values = list(np.amin(src_corners[:, :3], axis=0))
    min_values_t = list(np.amin(target_corners[:, :3], axis=0))

    start = np.asarray(b + lo - border, dtype=np.int)
    to = np.asarray(b + lo + np.ceil(rangos) + 1 + border, dtype=np.int)
    raw_cut = inp_src[start[0]: to[0], start[1]:to[1], start[2]:to[2], :].copy()
    target_cut = target_src[lo[0]:int(lo[0] + np.ceil(rangos_t[0]) + 1),
                 lo[1]:int(lo[1] + np.ceil(rangos_t[1]) + 1),
                 lo[2]:int(lo[2] + np.ceil(rangos_t[2]) + 1), :].copy()

    raw_patch = np.zeros((num_channels,) + patch_shape, dtype=np.float)
    target_patch = np.zeros((num_channels,) + patch_out_shape, dtype=np.float)

    if target_discrete_idx is None:
        channel_is_discrete = [True for _ in range(num_channels)]
    else:
        channel_is_discrete = [i in target_discrete_idx
                               for i in range(num_channels)]

    for k in range(num_channels):
        raw_patch[k] = map_coordinates_linear(raw_cut[:, :, :, k],
                                              src_coords_inp,
                                              min_values)
        if channel_is_discrete[k]:
            target_patch[k] = map_coordinates_nearest(target_cut[:, :, :, k],
                                                      src_coords_target,
                                                      min_values_t)
        else:
            target_patch[k] = map_coordinates_linear(target_cut[:, :, :, k],
                                                     src_coords_target,
                                                     min_values_t)

    if data_format == 'channels_last':
        raw_patch = np.moveaxis(raw_patch, 0, -1)
        target_patch = np.moveaxis(target_patch, 0, -1)

    return raw_patch, target_patch, lo
