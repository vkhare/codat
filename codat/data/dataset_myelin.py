""" Utility functions for tensorflow dataset creation.
taken from dataset.py
@author: Varun Khare <varun.khare@brain.mpg.de

tested with tf1.4

"""
from skimage import transform
import tensorflow as tf
from codat.data import augmentation
import numpy as np
import scipy.io as sio
import os
import glob
from typing import List, Tuple, Dict
import wkw
import cv2 as cv
from scipy import ndimage

class FCNData:
    """
    
    Dataset for 3D voxelwise network training (3D semantic segmentation).
    Input/target volumes are load into RAM (FCNData.cubes_in and
    FCNData.cubes_target) with an associated weight
    (FCNData.cube_sampling_weights) corresponding to the size of the target
    volume. Cubes are then sampled according to their weights and augmented
    using random rotations/flips.
    
    """
    def __init__(self, raw_input_path: str,
                 target_annotations_path: str,
                 seg_path: str,
                 bbox_train_file: str,
                 target_dtype='float32',
                 input_shape: List=None,
                 out_shape: List=None,
                 decay_prob_rate=0.95,
                 step_drop = 10000,
                 sample = None,
                 batch_size=1,
                 count=0,
                 border_diff= [44,44,18],
                 data_format='channels_last',
                 preproc_input=None,
                 preproc_target=None,
                 rotate=False,
                 flip=False,
                 class_weights: Dict[float, float]=None,
                 add_noise: Dict[str, float]=None,
                 mult_noise: Dict[str, float]=None,
                 cutout=None,
                 mixup=None,
                 seg = 0,
                 warp=None,
                 f_reject_sample=None,
                 num_calls=2,
                 prefetch=5):
        """
        
        Args:
            raw_input_path: string
                Path to folder with training cubes.
            target_annotations_path: 
                Same as input_files but for the target cubes (not needed if
                input files contains input and target volumes).
            bbox_coordinates_file: String
                coordinated fo boundign boxes
            target_dtype:
                Data type of the target volumes.
            input_shape: 
                Spatial shape of the input cubes that the dataset produces.
            out_shape: 
                Spatial shape of the output cubes that the dataset produces.
            batch_size: 
                The batch size for training.
            data_format: 
                'channels_first' or 'channels_last'
            preproc_input: 
                Function that is applied to the raw data after it was cropped to
                input_shape.
            preproc_target:
                Function that is applied to the label data after it was cropped
                to out_shape.
            rotate: 
                Performs random rotations by multiples of 90 degree in the first
                two data dimensions.
            flip:
                Performs random flips in the third data dimension.
            class_weights: 
                Dictionary of the form {class: weight} that assigns the weight
                to the corresponding class label.
            add_noise: dict or None
                Dictionary containing "mean" and "std" and "mean_std" for
                additive Gaussian noise.
            mult_noise: dict or None
                Dictionary containing the "mean" and "std" and "range_mean" for
                multiplicative Gaussian white noise.
            cutout: dict or None
                Dictionary with cutout parameters (mainly the cutout_size) or
                None for no cutout augmentation.
                see codat/data/augmentation.py
            mixup: None or dict
                Parameter for input mixup augmentation.
                'p': float specified the probability of performing mixup.
                'args': dict can specify the names arguments in
                    codat/data/augmentation.py
            warp: None or dict
                Parameter for elastic deformation augmentation.
                'p': probability of applying a elastic deformation
                'args': dictionary with elastic deformation hyperparameters:
                    see augmentation.warp_patch inputs
            f_reject_sample:
                Function of the form toReject = f_reject_sample(input, target)
                that decides whether to reject a current input/target pair after
                cropping, i.e. f_reject_sample should return true if the sample
                was rejected.
            num_calls: 
                Number of parallel calls (see dataset.map) for the python
                preprocessing pipeline.
            prefetch: 
                see dataset.prefetch
        """

        # the actual data cubes
        self.raw_data = wkw.Dataset.open(raw_input_path)
        self.target_annotations = wkw.Dataset.open(target_annotations_path)
        self.segmentation = wkw.Dataset.open(seg_path)

        self.bbox_train_file = bbox_train_file
        self.target_dtype = target_dtype
        self.sample_count = count
        self.strategy = sample
        self.decay_prob_rate = decay_prob_rate
        self.step_drop = step_drop
        n = (self.sample_count*batch_size)//self.step_drop
        if self.strategy == 'exp':
            self.curriculum = self.decay_prob_rate**n
        elif self.strategy == 'lin':
            self.curriculum = 1-(1-self.decay_prob_rate)*n*2/150
            self.curriculum = np.max([0,self.curriculum])
        else:
            self.curriculum = 0
        self.border_diff = border_diff
        self.big_cube_Size = np.array([624,624,142])
        self.cubes_in = {}
        self.cubes_target = {}
        self.filled_cubes= [1,2,5,7,8,12,13,14,15,29,53,54,61,82,88] + [0] #0 for new myelinated box
        self.new_myelin_boxes = [[105776, 165179, 4731]] #mapped to 0 in filled cubes
        self.empty_cubes = [6,9,10,21, 23, 30, 33, 38, 39, 49]
        self.extras = [16,24,26,28,36,37,
                  38,41,47,52,60,64,
                  65,73,74,76,85,86,
                  89,90,92,93,96,99]
        self.cubes_done = self.empty_cubes+self.filled_cubes+self.extras
        self.cubes_val_idx = [3,4,11,27,16]
        self.patch_shape = input_shape
        self.out_shape = out_shape
        self.batch_size = batch_size
        self.data_format = data_format
        self.preproc_input = preproc_input
        self.preproc_target = preproc_target
        self.rotate = rotate
        self.flip = flip
        self.seg = seg
        self.class_weights = class_weights
        self.num_calls = num_calls
        self.prefetch = prefetch
        self.f_reject_sample = f_reject_sample
        self.add_noise = {'mean': 0, 'std': 0, 'std_mean': 0}
        if add_noise is not None:
            self.add_noise.update(add_noise)
        self.mult_noise = {'mean': 1, 'std': 0, 'mean_range': 0}
        if mult_noise is not None:
            self.mult_noise.update(mult_noise)
        self.cutout = cutout
        self.mixup = {'p': 0, 'args': {}}
        if mixup is not None:
            self.mixup['p'] = mixup['p']
            if 'args' in mixup:
                self.mixup['args'].update(mixup['args'])
        self.warp = {'p': 0,
                     'args': {'lock_z': True,
                              'amount': 1,
                              'target_discrete_idx': None,
                              'perspective': False,
                              'red_z': 0.4}}
        if warp is not None:
            self.warp['p'] = warp['p']
            if 'args' in warp:
                self.warp['args'].update(warp['args'])
        self.load_cubes()
        self.print_settings()
        
    def sampling_strategy(self):
        if self.sample_count % self.step_drop == 0:
            if self.strategy == 'exp':
                self.curriculum *= self.decay_prob_rate
            elif self.strategy == 'lin':
                self.curriculum -= (1-self.decay_prob_rate)*2/150
                self.curriculum = np.max([0,self.curriculum])
            else:
                self.curriculum = 0
                   
                    

    def load_cubes(self):
        print("Loading data from {} file:".format(self.bbox_train_file))
        nodes = sio.loadmat(self.bbox_train_file)
        cubes_in = nodes['final'].T
        if len(cubes_in) == 0:
            raise RuntimeError('No input cubes were loaded.')
        else :
            print('total number of cubes are',len(cubes_in))
        for i in self.cubes_done: 
            if i == 0:
                coor = np.array(self.new_myelin_boxes[0])
            else:
                coor = np.min(cubes_in[i,:,:],axis=1)[:3]-1
#             #downsample
#             input_cube = self.raw_data.read(coor//np.array([2,2,1])-self.border_diff,
#                                             self.big_cube_Size//np.array([2,2,1])+2*self.border_diff)[0]
            input_cube = self.raw_data.read(coor-self.border_diff,
                                            self.big_cube_Size+2*self.border_diff)[0]
            target_cube = self.target_annotations.read(coor,self.big_cube_Size)[0]
#             target_cube = transform.resize(target_cube,self.big_cube_Size//np.array([2,2,1]),
#                              anti_aliasing=False,order=0,
#                              preserve_range=True).astype(np.int32)
            ic,tc = self._add_channel_dim([input_cube,target_cube])
            ic = ic.astype(np.float32)
            if self.seg:
#                 seg = self.segmentation.read(coor//np.array([2,2,1])-self.border_diff,
#                                              self.big_cube_Size//np.array([2,2,1])+2*self.border_diff)[0]
                seg = self.segmentation.read(coor-self.border_diff,
                                             self.big_cube_Size+2*self.border_diff)[0]
                seg = cv.Laplacian(np.float32(seg),cv.CV_32F)
                seg[np.abs(seg)>0] = 1
                seg[np.abs(seg)<=0] = 0
#                 seg = ndimage.distance_transform_edt(1-seg)
#                 seg = (seg-4)/2.5
                seg = self._add_channel_dim([seg]) 
                dim = 3 if self.data_format == 'channels_last' else 0
                self.cubes_in[i] = np.concatenate((ic,seg[0]),axis=dim)
            else:
                self.cubes_in[i] = ic
            self.cubes_target[i] = tc
            print('cube ',i,'loaded')
        print("Loading of {} file finished.".format(self.bbox_train_file))

        

    def _validate_border_size(self, raw, target):
        aa = lambda x: np.asarray(x)
        get_shape = lambda x: np.asarray(x.shape[:3])
        b = aa(self.patch_shape) - aa(self.out_shape)
        not_enough_border=False
        raw_shape = get_shape(raw)
        target_shape = get_shape(target)
        b1 = raw_shape - target_shape
        if np.any(b > b1):
            not_enough_border=True
        if not_enough_border:
            print("WARNING: The border around the training cubes is "
                  "too small to allow access to all labels.")
            print("Required border {}. Actual border {}.".format(b, b1))

    def _add_channel_dim(self,inputs):

        def add_channels(cube, dim):
            if cube.ndim == 3:
                cube = np.expand_dims(cube, dim)
            return cube

        if self.data_format == 'channels_last':
            dim = 4
        else:
            dim = 1

        fmap = lambda x: add_channels(x, dim)
        return list(map(fmap,inputs))

    def __iter__(self):
        return self

    def as_generator(self):
        while True:
            flag = np.random.random_sample() < self.curriculum
            yield self.next(flag)

    def next(self,flag):
        """

        Returns:
            next_element: the index of the next cube
        """
        if flag:
            idx = self.filled_cubes[np.random.choice(len(self.filled_cubes))]
        else:
            if np.random.random_sample() < 0.591:
                idx = self.filled_cubes[np.random.choice(len(self.filled_cubes))]
            else:
                idx = (self.empty_cubes+self.extras)[np.random.choice(len(self.empty_cubes+self.extras))]
        return (idx,flag)

    def _get_cube(self, idx):
        return self.cubes_in[idx], self.cubes_target[idx]
        

    def _sample_mixup_cube(self):
        cube_raw, cube_target = self._get_cube(self.next())
        do_sample = True
        while do_sample:
            raw, target = sample_patch(self.patch_shape, self.out_shape,
                                       cube_raw, cube_target, self.data_format)
            if self.f_reject_sample is not None:
                do_sample = self.f_reject_sample(raw, target)
            else:
                do_sample = False

        raw = np.asarray(raw, dtype=np.float32)

        if self.preproc_input is not None:
            raw = self.preproc_input(raw)

        if self.preproc_target is not None:
            target = self.preproc_target(target)

        if self.rotate:
            raw, target = rotate_data(raw, target,
                                         data_format=self.data_format)
        if self.flip:
            raw, target = flip_data(raw, target,
                                       data_format=self.data_format)
        return raw, target

    def _get_training_example(self, idx,flag):
        cube_raw, cube_target = self._get_cube(idx)
        do_sample = True
        while do_sample:
            raw, target = sample_patch(self.patch_shape, self.out_shape,
                                       cube_raw, cube_target,flag,
                                       data_format=self.data_format,
                                       warp=self.warp)
            if self.f_reject_sample is not None:
                do_sample = self.f_reject_sample(raw, target)
            else:
                do_sample = False
        raw = np.asarray(raw, dtype=np.float32)
        if self.preproc_input is not None:
            raw = self.preproc_input(raw)

        if self.preproc_target is not None:
            target = self.preproc_target(target)

        if self.rotate:
            raw, target, _ = rotate_data(raw, target,
                                         data_format=self.data_format)
        if self.flip:
            raw, target, _ = flip_data(raw, target,
                                       data_format=self.data_format)

        mixup_done = False
        if self.mixup['p'] > 0 and (np.random.rand() <= self.mixup['p']):
            mixup_done = True
            raw2, target2 = self._sample_mixup_cube()
            t_copy = target.copy()
            raw, target, l = augmentation.input_mixup(raw, target, raw2,
                                                      target2,
                                                      **self.mixup['args'])

        target = np.asarray(target, dtype=self.target_dtype)
        raw = apply_additive_noise(raw, **self.add_noise)
        raw = apply_mult_noise(raw, **self.mult_noise)
        if self.cutout is not None:
            raw = augmentation.cutout(raw, **self.cutout)
        self.sample_count += 1
        #decay after every step_drop steps by decay rate
        self.sampling_strategy()
        if self.class_weights is not None:
            
            weights = np.ones(target.shape, dtype=np.float32)
            for (c, w) in self.class_weights.items():
                if mixup_done:
                    # same convex combination for weights
                    weights[(t_copy == c) & (target2 == c)] = w
                    weights[(t_copy == c) & (target2 != c)] = l * w
                    weights[(t_copy != c) & (target2 == c)] = (1 - l) * w
                else:
                    weights[target == c] = w
            weights = weights * weights.size / weights.sum()
            weights = weights.astype(np.float32)

            return raw, target, weights
        else:
            return raw, target

    def _set_tensor_shape(self, raw, target, weights=None):
        bs = self.batch_size
        ps = self.patch_shape
        o_shp = self.out_shape
        if self.data_format == 'channels_last':
            c_in = 2 if self.seg else 1
            c_out = 1
            raw = tf.ensure_shape(raw, np.concatenate(([bs, ], ps, [c_in, ])))
            target= tf.ensure_shape(target, np.concatenate(([bs, ], o_shp, [c_out, ])))
            if weights is not None:
                tf.ensure_shape(weights,np.concatenate(([bs, ], o_shp, [c_out, ])))
        else:
            #TODO: for now hardcoded to 1 will change later
            c_in = 2 if self.seg else 1
            c_out = 1
            raw = tf.ensure_shape(raw,np.concatenate(([bs, c_in], ps)))
            target = tf.ensure_shape(target,np.concatenate(([bs, c_out], o_shp)))
            if weights is not None:
                tf.ensure_shape(weights,np.concatenate(([bs, c_out], o_shp)))

        if weights is not None:
            return raw, target, weights
        else:
            return raw, target

    def as_tf_dataset(self):
        dset = tf.data.Dataset.from_generator(self.as_generator, (tf.int32,tf.bool))
        mf = lambda i,flag: self._get_training_example(i,flag)
        tf_out_dtype = tf.as_dtype(self.target_dtype)

        if self.class_weights is None:
            outputs = [tf.float32, tf_out_dtype]
        else:
            outputs = [tf.float32, tf_out_dtype, tf.float32]

        dset = dset.map(lambda i,flag: tuple(tf.py_func(mf, [i,flag], outputs)),
                        num_parallel_calls=self.num_calls)
        # NOTE(amotta): The `drop_remainder` flag is set, so that TensorFlow
        # correctly sets the batch size in the output shape. Since we have an
        # "endless" stream of trainig samples, we don't actually drop anything.
        dset = dset.batch(self.batch_size, drop_remainder=True)
        dset = dset.map(lambda *args: tuple(self._set_tensor_shape(*args)),
                        num_parallel_calls=self.num_calls)
        dset = dset.prefetch(self.prefetch)
        return dset

    def get_one_shot_iterator_outputs(self):
        """
        
        Returns:
            data: Tuple
                Tuple consisting of (raw, targets) or (raw, targets, weights)
                if self.class_weights is not none
                The dimensions for each cube is given by
                (batch_size, x, y, z, channels)
        """
        dset = self.as_tf_dataset()
        train_it = dset.make_one_shot_iterator()
        data = train_it.get_next()
        data = self._set_tensor_shape(*data)
        return data

    def print_settings(self):
        print()
        print('Dataset options:')
        if self.class_weights is not None:
            print('Class weights: {}'.format(self.class_weights))

        print('Random rotations by 90 deg: {}'.format(self.rotate))
        print('Random flips along z-direction: {}'.format(self.flip))
        print('Additive noise settings: {}'.format(self.add_noise))
        print('Multiplicative noise settings: {}'.format(self.mult_noise))
        print('Warping settings: {}'.format(self.warp))
        if self.cutout is not None:
            print('Using cutout with settings: {}.'.format(self.cutout))
        if self.f_reject_sample is not None:
            print('Using custom rejection function \'{}\'.'
                  .format(self.f_reject_sample.__name__))
        if self.preproc_input is not None:
            print('Using custom input preprocessing function \'{}\'.'
                  .format(self.preproc_input.__name__))
        if self.preproc_target is not None:
            print('Using custom target preprocessing function \'{}\'.'
                  .format(self.preproc_target.__name__))
        print()


def sample_patch(patch_shape, out_shape, raw, target, flag,
                 data_format='channels_last',
                 warp=None):
    aa = lambda x: np.asarray(x, dtype=np.uint32)
    raw_shape = aa(raw.shape)
    target_shape = aa(target.shape)
    patch_shape = aa(patch_shape)
    out_shape = aa(out_shape)

    assert np.all(patch_shape >= out_shape)
    border_size = (patch_shape - out_shape) // 2 # between input and output
    assert np.all(raw_shape[:-1] >= target_shape[:-1])
    b = (raw_shape[:-1] - target_shape[:-1]) // 2 # actual available border

    # implementation of warping
    if warp is not None and (np.random.rand() <= warp['p']):
        wraw, wtarget, _ = augmentation.warp_patch(patch_shape, raw, target,
                                                   patch_out_shape=out_shape,
                                                   **warp['args'],
                                                   data_format=data_format)
        if wtarget is not None:
            # return result if not failed otherwise do non-warped sampling
            return wraw, wtarget

    assert np.all(target_shape[:3] >= out_shape[:3])
    off_to = (target_shape[:3] - out_shape[:3]) + 1

    offset = np.zeros((3,), dtype=np.uint32)
    true_positives = np.where(target != 0)

    if flag and len(true_positives[0]) > 5:
#     if len(true_positives[0]>0):
        # sample patch around myelin annotations
        # atleast 5 positive voxels should be present in the sampled cube
        sample_idx = np.random.randint(len(true_positives[0]))
        offset = np.array([i[sample_idx] for i in true_positives][:-1]) - [40,40,10]
        offset[offset<0] = 0
        overflow_end = offset + out_shape - target_shape[:3]+1
        overflow_mask = overflow_end > 0
        if np.any(overflow_mask): 
            offset[overflow_mask] = offset[overflow_mask]-overflow_end[overflow_mask]
    else:    
        # random offset in full target volume
        offset[0] = np.random.randint(0, off_to[0], 1, dtype=offset.dtype)
        offset[1] = np.random.randint(0, off_to[1], 1, dtype=offset.dtype)
        offset[2] = np.random.randint(0, off_to[2], 1, dtype=offset.dtype)

    ts = aa(out_shape)
    input_from = b + offset - border_size
    input_to = b + offset + border_size + ts
    if data_format == 'channels_last':
        raw = raw[input_from[0]:input_to[0],
                  input_from[1]:input_to[1],
                  input_from[2]:input_to[2],:].copy()
        target = target[offset[0]:(offset[0] + ts[0]),
                        offset[1]:(offset[1] + ts[1]),
                        offset[2]:(offset[2] + ts[2]),:].copy()
    else:
        raw = raw[:,
                  input_from[0]:input_to[0],
                  input_from[1]:input_to[1],
                  input_from[2]:input_to[2]].copy()
        target = target[:,
                        offset[0]:(offset[0] + ts[0]),
                        offset[1]:(offset[1] + ts[1]),
                        offset[2]:(offset[2] + ts[2])].copy()
    return raw, target


def rotate_data(raw, target,weights=None,data_format='channels_last'):
    k = np.random.randint(0, 5, 1)
    if data_format == 'channels_last':
        axes = (0, 1)
    else:
        axes = (1, 2)
    raw = np.rot90(raw, k, axes)
    target = np.rot90(target, k, axes)
    if weights is not None:
        weights = np.rot90(weights, k, axes)
    return raw, target, weights


def flip_data(raw, target,weights=None, data_format='channels_last'):
    if np.random.rand(1) > 0.5:
        if data_format == 'channels_last':
            dim = 2
        else:
            dim = 3
        raw = np.flip(raw, dim)
        target = np.flip(target, dim)
        if weights is not None:
            target = np.flip(weights, dim)
    return raw, target, weights


def apply_additive_noise(raw, mean=0, std=0, std_mean=0):
    if std_mean > 0:
        mean = mean + np.random.randn(1) * std_mean
    if std == 0:
        raw[...,0] = raw[...,0] + mean
    elif std > 0:
        raw[...,0] = raw[...,0] + (mean + np.random.randn(*raw[...,0].shape) * std)
    return raw.astype(np.float32)


def apply_mult_noise(raw, mean=1, std=0, mean_range=0):
    if mean_range > 0:
        mean = mean + np.random.uniform(-mean_range / 2., mean_range / 2., 1)
    if std == 0:
        raw[...,0] = raw[...,0] * mean
    elif std > 0:
        raw[...,0] = raw[...,0] * (mean + np.random.randn(*raw[...,0].shape) * std)
    return raw.astype(np.float32)


class FileLoader:
    def __init__(self, files, weights=None):
        if type(files) == list:
            self.files = files
        else:
            self.files = glob.glob(files)
        if weights is not None:
            weights = np.asarray(weights, np.float32)
            weights = weights / weights.sum()
            self.weights = weights
            assert self.weights.size == len(self.files), "The weights must be" \
                " specified for each file"
        else:
            self.weights = np.ones(len(self.files), dtype=np.float32) /\
                           len(self.files)

    def __iter__(self):
        return self

    def as_generator(self):
        while True:
            yield self.next()

    def next(self):
        """
        
        Returns:
            next_element: the next filename as a bytestring
        """
        idx = np.random.choice(len(self.files), p=self.weights)
        return self.files[idx].encode()

    def as_tf_dataset(self):
        return tf.data.Dataset.from_generator(self.as_generator, (tf.string))

