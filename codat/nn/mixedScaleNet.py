"""" Mixed-scale like fully convolutional net.

Based on Pelt, Sethian, 2018, PNAS
and Grais et al., 2017, arxiv

@author: Benedikt Staffler <benedikt.staffler@brain.mpg.de

tested with tf1.4

"""

import tensorflow as tf
from codat.nn.fcn import FCN
from codat.nn.util import crop_to_smallest, tanh_scaled, concat_channels
import numpy as np
from typing import List, Tuple


def mScale_layer(inputs: tf.Tensor,
                 filter_sizes: List[List[int]],
                 feature_maps: List[int],
                 dilation_rate: List[Tuple[int, int, int]],
                 activation=tanh_scaled,
                 data_format='channels_last',
                 dense_inputs=None,
                 dropout_rate=0.,
                 is_training=True,
                 name=None,
                 reuse=None) -> tf.Tensor:
    """
    Basic multi-resolution layer building block that consists of parallel
    pathways of convolutional layers for which the outputs are concatenated at
    the end.
    Args:
        inputs: Input tensor.
        filter_sizes: The outer list refers to the parallel pathways in the
            layer, i.e. filter_sizes[i] refers to one pathway in the layer.
            Each pathway can consist of successive convolutions with size
            specified in the nested lists, i.e. filter_sizes[i][j] refers to the
            j-th filter in the i-th pathway. Successive convolutions of a
            pathway are as a residual block.
        feature_maps: The feature maps for each parallel pathway. Along a
            pathway the feature maps are kept constant.
        dilation_rate: The dilation rate for the corresponding pathway.
        activation: The non-linearity used for all layers.
        data_format: see tf.layers.conv3d
        dense_inputs: List of tensors that are concatenated with inputs.
        dropout_rate: Dropout rate.
        is_training: Flag to indicate that no dropout should be used.
        name: see tf.layers.conv3d
        reuse: see tf.layers.conv3d

    Returns:
        output: Output tensor with a total of feature_maps.sum() feature maps.
    """

    kernel_init = tf.contrib.layers.xavier_initializer()

    if dense_inputs is not None:
        with tf.variable_scope('{}/dense_inputs'.format(name)):
            inputs = crop_to_smallest([inputs, ] + dense_inputs,
                                      data_format=data_format)
            inputs = concat_channels(inputs, data_format=data_format)
    print('Input shape: {}'.format(inputs.get_shape().as_list()))
    if dropout_rate > 0:
            inputs = tf.layers.dropout(inputs, dropout_rate,
                                       training=is_training,
                                       name='{}/dropout'.format(name))
    print('Dropout rate: {}'.format(dropout_rate))


    out = [tf.Tensor] * len(feature_maps)
    for path in range(len(feature_maps)):

        if name is not None:
            path_name = '{}/path_{}'.format(name, path)
        else:
            path_name = 'path_{}'.format(path)

        # embedding to pathway feature map dimension
        with tf.variable_scope(path_name):
            out[path] = tf.layers.conv3d(inputs, feature_maps[path],
                                         filter_sizes[path],
                                         dilation_rate=dilation_rate[path],
                                         activation=activation,
                                         kernel_initializer=kernel_init,
                                         data_format=data_format,
                                         reuse=reuse)

    if name is not None:
        with tf.variable_scope('{}/output'.format(name)):
            out = crop_to_smallest(out, data_format=data_format)
            y = concat_channels(out, data_format=data_format)
    else:
        out = crop_to_smallest(out, data_format=data_format)
        y = concat_channels(out, data_format=data_format)

    return y


def output_layer(inputs: tf.Tensor,
                 output_channels: int,
                 activation,
                 data_format='channels_last',
                 reuse=None):
    with tf.variable_scope('output_layer'):
        kernel_init = tf.contrib.layers.xavier_initializer()
        logits = tf.layers.conv3d(inputs, output_channels, (1, 1, 1),
                                  activation=None,
                                  kernel_initializer=kernel_init,
                                  data_format=data_format,
                                  reuse=reuse)
        out = activation(logits)

    return out, logits


class MixedScaleNetwork(FCN):
    """
    Neural network class based on Pelt, Sethian, 2018 and Grais et al., 2017.
    """

    def __init__(self,
                 channels_out: int,
                 loss: str = 'squared',
                 act_hidden=None,
                 act_out=None,
                 data_format: str = 'channels_last',
                 dropout_rate: List[float] = None,
                 feature_maps: List[int] = None,
                 filter_sizes: List[List[List[int]]] = None,
                 dilation_rate: List[List[List[int]]] = None,
                 is_training=None,
                 dense_net=None):
        FCN.__init__(self)
        if type(feature_maps[0]) is not list:
            feature_maps = [[feature_maps[i]] * len(filter_sizes[i]) for i in
                            range(len(feature_maps))]
        self.num_layer = len(feature_maps)
        self.channels_out = channels_out
        self.loss_name = loss
        self.input_tensor = None
        self.output_tensor = None
        self.label_tensor = None
        self.logit_tensor = None
        self.loss_tensor = None
        self.weight_tensor = None
        self.is_training = True
        self.session = None
        self.layers = (len(feature_maps) + 1) * [None]
        if dense_net is not None:
            self.dense_net = dense_net
        else:
            self.dense_net = False
        self.feature_maps = feature_maps
        for i in range(len(filter_sizes)):
            for j in range(len(filter_sizes[i])):
                if isinstance(filter_sizes[i][j], int):
                    filter_sizes[i][j][0] = 3 * [filter_sizes[i][j]]

        self.filter_sizes = filter_sizes
        for i in range(len(dilation_rate)):
            for j in range(len(dilation_rate[i])):
                if isinstance(dilation_rate[i][j], int):
                    dilation_rate[i][j] = 3 * [dilation_rate[i][j]]
        self.dilation_rate = dilation_rate
        if is_training is None:
            self.is_training = True
        else:
            self.is_training = is_training

        # non-linearities
        if act_hidden is None:
            self.act_hidden = tanh_scaled
        else:
            self.act_hidden = act_hidden
        if act_out is None:
            if self.loss_name == 'squared':
                act_out = tanh_scaled
            elif self.loss_name == 'cross-entropy':
                act_out = tf.nn.softmax
            else:
                raise RuntimeError('Specify act_out.')
        self.act_out = act_out
        self.data_format = data_format

        # dropout
        if dropout_rate is None:
            self.dropout_rate = [0] * self.num_layer
        elif len(dropout_rate) == 1:
            self.dropout_rate = dropout_rate * self.num_layer
        else:
            assert len(dropout_rate) == self.num_layer, \
                'Wrong dropout specified.'
            self.dropout_rate = dropout_rate

    def build_model(self, inputs, labels=None, weights=None):
        """
        Create the computational graph.
        Args:
            inputs: list or tensor
                Inputs tensor or list of input size. In the latter case a
                placeholder for the labels is generated.
                (see also create_pl_tensor).
            labels:  tensor
                Label tensor. If none then a placeholder with the correct shape
                will be created
            weights: tensor
                Label weight tensor.
        """

        self.build_inference_model(inputs)
        if weights is not None:
            self.weight_tensor = weights

        if labels is not None:
            self.label_tensor = labels
        else:
            self.create_pl_label_tensor()
        self.loss_tensor = self.loss()

    def build_inference_model(self, inputs=None):
        if inputs is not None:
            self._set_input(inputs)
        self.layers[0] = self.input_tensor
        h = self.layers[0]
        print('Dense net: {}'.format(self.dense_net))
        print('Input shape: {}'.format(h.get_shape().as_list()))
        for i in range(len(self.feature_maps)):
            if self.dense_net and i > 0:
                tp = self.layers[:i]
            else:
                tp = None
            print('Layer {}'.format(i))
            h = mScale_layer(inputs=h, filter_sizes=self.filter_sizes[i],
                             feature_maps=self.feature_maps[i],
                             dilation_rate=self.dilation_rate[i],
                             activation=self.act_hidden,
                             name='layer_{}'.format(i),
                             data_format=self.data_format,
                             dense_inputs=tp,
                             reuse=self.reuse,
                             dropout_rate=self.dropout_rate[i],
                             is_training=self.is_training)
            lyr_str = ''
            for j in range(len(self.filter_sizes[i])):
                lyr_str = lyr_str + 'FMs: {}; Filter size {}; Dilation {}\n'\
                    .format(self.feature_maps[i][j], self.filter_sizes[i][j],
                            self.dilation_rate[i][j])
            print(lyr_str)
            self.layers[i+1] = h
        output_tensor, self.logit_tensor = output_layer(h, self.channels_out,
            self.act_out, data_format=self.data_format, reuse=self.reuse)
        self._set_output(output_tensor)
        self.calculate_border_size()
        print('Output shape: {}'.format(output_tensor.get_shape().as_list()))
        print('Total number of parameters: {}'.format(self._num_params()))
        return self.output_tensor

    def calculate_border_size(self, inputs=None):
        fs = np.asarray(self.filter_sizes)
        dl = np.asarray(self.dilation_rate)
        bd = (fs-1)*dl
        self.border_size = np.sum(np.max(bd, axis=1), axis=0)

    def calculate_border_brute_force(self, inputs=None):
        self.calculate_border_size()