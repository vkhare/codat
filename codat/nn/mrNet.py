""" Multi-resolution fully convolutional net.

@author: Benedikt Staffler <benedikt.staffler@brain.mpg.de

tested with tf1.4

"""

import tensorflow as tf
from codat.nn.fcn import FCN
from codat.nn.util import crop, crop_to_smallest, tanh_scaled, \
    concat_channels
import numpy as np
import os
import codat.training.regularizer as regularizer
from typing import List, Tuple


def mr_layer(inputs: tf.Tensor,
             filter_sizes: List[List[Tuple[int, int, int]]],
             feature_maps: List[int],
             dilation_rate: List[Tuple[int, int, int]],
             activation=tanh_scaled,
             data_format='channels_last',
             name=None,
             reuse=None,
             use_res_paths=True,
             padding="valid",
             kernel_regularizer=None,
             bias_regularizer=None) -> tf.Tensor:
    """
    Basic multi-resolution layer building block that consists of parallel
    pathways of convolutional layers for which the outputs are concatenated at
    the end.
    Args:
        inputs: Input tensor.
        filter_sizes: The outer list refers to the parallel pathways in the
            layer, i.e. filter_sizes[i] refers to one pathway in the layer.
            Each pathway can consist of successive convolutions with size
            specified in the nested lists, i.e. filter_sizes[i][j] refers to the
            j-th filter in the i-th pathway. Successive convolutions of a
            pathway are as a residual block.
        feature_maps: The feature maps for each parallel pathway. Along a
            pathway the feature maps are kept constant.
        dilation_rate: The dilation rate for the corresponding pathway.
        activation: The non-linearity used for all layers.
        data_format: see tf.layers.conv3d
        name: see tf.layers.conv3d
        reuse: see tf.layers.conv3d
        use_res_paths: Consecutive convolutions within a path are implemented as
            a residual block. If the input has less feature maps then the output
            of the path a 1x1x1 convolution is used to increase the dimensions.
        padding: see tf.layers.conv3d
        kernel_regularizer: see tf.layers.conv3d
        bias_regularizer: see tf.layers.conv3d

    Returns:
        output: Output tensor with a total of feature_maps.sum() feature maps.
    """

    kernel_init = tf.contrib.layers.xavier_initializer()
    inputs_shape = inputs.get_shape().as_list()
    if data_format == 'channels_last':
        inputs_fm = inputs_shape[4]
    else:
        inputs_fm = inputs_shape[1]

    out = [tf.Tensor] * len(feature_maps)
    for path in range(len(feature_maps)):

        if name is not None:
            path_name = '{}/path_{}'.format(name, path)
        else:
            path_name = 'path_{}'.format(path)

        # embedding to pathway feature map dimension
        with tf.variable_scope(path_name):
            if inputs_fm < feature_maps[path] and len(filter_sizes[path]) > 1 \
                    and use_res_paths:
                with tf.variable_scope('embedding'):
                    emb = tf.layers.conv3d(inputs, feature_maps[path],
                                           (1, 1, 1),
                                           activation=activation,
                                           kernel_initializer=kernel_init,
                                           data_format=data_format,
                                           reuse=reuse,
                                           padding=padding,
                                           kernel_regularizer=kernel_regularizer,
                                           bias_regularizer=bias_regularizer)
            else:
                emb = inputs

            # successive convolutions
            h = emb
            for j in range(len(filter_sizes[path])):
                with tf.variable_scope('conv_{}'.format(j)):
                    h = tf.layers.conv3d(h, feature_maps[path],
                                         filter_sizes[path][j],
                                         dilation_rate=dilation_rate[path],
                                         activation=activation,
                                         kernel_initializer=kernel_init,
                                         data_format=data_format,
                                         reuse=reuse,
                                         padding=padding,
                                         kernel_regularizer=kernel_regularizer,
                                         bias_regularizer=bias_regularizer)

            # residual connection if several convolutions were used
            with tf.variable_scope('concat', reuse=reuse):
                if use_res_paths and len(filter_sizes[path]) > 1:
                    h = h + crop(emb, h.get_shape().as_list())
                out[path] = h

    if name is not None:
        with tf.variable_scope('{}/output'.format(name)):
            out = crop_to_smallest(out, data_format=data_format)
            y = concat_channels(out, data_format=data_format)
    else:
        out = crop_to_smallest(out, data_format=data_format)
        y = concat_channels(out, data_format=data_format)

    return y


def def_high_med_res_layer(inputs: tf.Tensor,
                           data_format='channels_last',
                           feature_maps=(16, 16),
                           reuse=None,
                           activation=None,
                           padding="valid",
                           kernel_regularizer=None,
                           bias_regularizer=None) -> tf.Tensor:
    """
    Default mr layer for our L4 data.
    Args:
        inputs: Input tensor.
        data_format: see tf.layers.conv3d
        feature_maps: Feature maps along the high and med res path.
        activation: see tf.layers.conv3d
        reuse: see tf.layers.conv3d
        padding: see tf.layers.conv3d
        kernel_regularizer: see tf.layers.conv3d
        bias_regularizer: see tf.layers.conv3d

    Returns:
        y: Output tensor.
    """
    y = mr_layer(inputs, [[(3, 3, 2), (3, 3, 2)], [(3, 3, 3)]], feature_maps,
                 [(1, 1, 1), (2, 2, 1)], data_format=data_format, reuse=reuse,
                 padding=padding, kernel_regularizer=kernel_regularizer,
                 bias_regularizer=bias_regularizer, activation=activation)
    return y


def def_low_res_layer(inputs: tf.Tensor, data_format='channels_last',
                      num_fms=(16, ), reuse=None, activation=None,
                      padding="valid", kernel_regularizer=None,
                      bias_regularizer=None) -> tf.Tensor:
    y = mr_layer(inputs, [[(3, 3, 3)]], num_fms,
                 [(4, 4, 2)], data_format=data_format, reuse=reuse,
                 padding=padding, kernel_regularizer=kernel_regularizer,
                 bias_regularizer=bias_regularizer, activation=activation)
    return y


def nested_mr_layer(inputs,
                    layers,
                    data_format='channels_last',
                    name=None):
    out = [tf.Tensor] * len(layers)
    for i in range(len(layers)):
        h = inputs
        for j in range(len(layers[i])):
            if name is not None:
                with tf.variable_scope(name + '/path_{}/mr_{}'.format(i, j)):
                    h = layers[i][j](h)
            else:
                h = layers[i][j](h)
        out[i] = h

    if name is not None:
        with tf.variable_scope(name):
            out = crop_to_smallest(out, data_format=data_format)
            y = concat_channels(out, data_format=data_format)
    else:
        out = crop_to_smallest(out, data_format=data_format)
        y = concat_channels(out, data_format=data_format)

    # out = crop_to_smallest(out, data_format=data_format)
    # y = concat_channels(out, data_format=data_format)
    return y


def def_high_med_low_res_layer(inputs: tf.Tensor,
                               name=None,
                               fms=16,
                               data_format='channels_last',
                               dropout_rate=None,
                               is_training=True,
                               mr_fms=(16, 16, 16),
                               reuse=None,
                               activation=None,
                               padding="valid",
                               kernel_regularizer=None,
                               bias_regularizer=None) -> tf.Tensor:
    if dropout_rate is not None and dropout_rate > 0:
        inputs = tf.layers.dropout(inputs, dropout_rate, training=is_training,
                                   name=name + '/dropout')

    def l_high_med(x):
        return def_high_med_res_layer(x, data_format=data_format,
                                      feature_maps=mr_fms[0:2],
                                      reuse=reuse,
                                      activation=activation,
                                      padding=padding,
                                      kernel_regularizer=kernel_regularizer,
                                      bias_regularizer=bias_regularizer)
    def l_low(x):
        return def_low_res_layer(x, data_format=data_format,
                                 num_fms=[mr_fms[2], ],
                                 reuse=reuse,
                                 activation=activation,
                                 padding=padding,
                                 kernel_regularizer=kernel_regularizer,
                                 bias_regularizer=bias_regularizer)

    y = nested_mr_layer(inputs, [[l_high_med, l_high_med], [l_low]], name=name,
                        data_format=data_format)

    # add convolution to further reduce the number of feature maps between
    # layers
    init = tf.contrib.layers.xavier_initializer()
    y = tf.layers.conv3d(y, fms, (3, 3, 1), activation=tanh_scaled,
                         kernel_initializer=init,
                         name=name + '/reduce',
                         data_format=data_format,
                         reuse=reuse,
                         padding=padding,
                         kernel_regularizer=kernel_regularizer,
                         bias_regularizer=bias_regularizer)
    if activation is None:
        act = "linear"
    else:
        act = activation.__name__
    print('Adding mr layer: {} ({} fms out, {} fms path, activation:{}, '
          'dropout {})'.format(y.get_shape().as_list(), fms, mr_fms, act,
                               dropout_rate if is_training else 0))
    return y


def output_layer(inputs: tf.Tensor,
                 output_channels: int,
                 activation,
                 data_format='channels_last',
                 reuse=None,
                 kernel_regularizer=None,
                 bias_regularizer=None):
    with tf.variable_scope('output_layer'):
        kernel_init = tf.contrib.layers.xavier_initializer()
        logits = tf.layers.conv3d(inputs, output_channels, (1, 1, 1),
                                  activation=None,
                                  kernel_initializer=kernel_init,
                                  data_format=data_format,
                                  reuse=reuse,
                                  kernel_regularizer=kernel_regularizer,
                                  bias_regularizer=bias_regularizer)
        if activation is not None:
            if isinstance(activation, (list, tuple)):
                assert(len(activation) == output_channels)
                with tf.variable_scope('output_activation'):
                    outs = tf.split(logits, output_channels, axis=-1)
                    for i, act in enumerate(activation):
                        if act is None or act == 'linear':
                            pass
                        else:
                            outs[i] = act(outs[i])
                out = tf.concat(outs, axis=4)
            else:
                out = activation(logits)
        else:
            out = logits

    return out, logits


def t_shape(t):
    return t.get_shape().as_list()


class DefaultMRNet(FCN):

    def __init__(self,
                 num_layer: int,
                 channels_out: int,
                 loss: str = 'squared',
                 act_hidden=None,
                 act_out=None,
                 data_format: str = 'channels_last',
                 dropout_rate: List[float] = None,
                 feature_maps: List[int] = None,
                 mr_path_fms: List[List[int]] = ([16, 16, 16], ),
                 padding=None,
                 kernel_regularizer=None,
                 bias_regularizer=None):
        FCN.__init__(self)
        self.num_layer = num_layer
        self.channels_out = channels_out
        self.loss_name = loss
        self.input_tensor = None
        self.output_tensor = None
        self.label_tensor = None
        self.logit_tensor = None
        self.loss_tensor = None
        self.weight_tensor = None
        self.is_training = True
        self.session = None
        self.mr_path_fms = None
        if padding is None:
            self.padding = "valid"
        else:
            self.padding = padding

        # linear option due to legacy handling of none input
        if act_out is None:
            if self.loss_name == 'squared':
                act_out = tanh_scaled
            elif self.loss_name == 'one_sided_squared':
                act_out = tanh_scaled
            elif self.loss_name == 'cross-entropy':
                act_out = tf.nn.softmax
            elif self.loss_name == 'cross-entropy-reg':
                act_out = tf.nn.softmax
            elif self.loss_name == 'dice':
                act_out = tf.nn.softmax
            elif self.loss_name == 'full_gaussian_nll':
                act_out = None  # linear activation
            else:
                raise RuntimeError('Specify act_out.')
        elif act_out == "linear":
            act_out = None
        self.act_out = act_out
        self.data_format = data_format

        # act hidden
        if act_hidden is None:
            self.act_hidden = tanh_scaled
        else:
            self.act_hidden = act_hidden

        # dropout
        if dropout_rate is None:
            self.dropout_rate = [0] * num_layer
        elif len(dropout_rate) == 1:
            self.dropout_rate = dropout_rate * num_layer
        else:
            assert len(dropout_rate) == num_layer, 'Wrong dropout specified.'
            self.dropout_rate = dropout_rate

        # feature maps
        if feature_maps is None:
            self.feature_maps = [16] * num_layer
        elif len(feature_maps) == 1:
            self.feature_maps = feature_maps * num_layer
        else:
            assert len(feature_maps) == num_layer, 'Wrong feature_maps' \
                                                   'specified.'
            self.feature_maps = feature_maps

        # feature maps in each mr_path of a layer
        if len(mr_path_fms) == 1:
            self.mr_path_fms = [mr_path_fms[0] for _ in range(num_layer)]
        elif mr_path_fms is not None:
            assert len(mr_path_fms) == num_layer, 'Wrong resolution pathway ' \
                                                  'fms specified'
            self.mr_path_fms = mr_path_fms
        self.kernel_regularizer = kernel_regularizer
        self.bias_regularizer = bias_regularizer

    def inference(self, inputs=None):
        if inputs is not None:
            self._set_input(inputs)
        h = self.input_tensor
        print('Input shape: {} (data format: {})'
              .format(h.get_shape().as_list(), self.data_format))
        for i in range(self.num_layer):
            h = def_high_med_low_res_layer(h, name='layer_{}'.format(i),
                                           data_format=self.data_format,
                                           fms=self.feature_maps[i],
                                           dropout_rate=self.dropout_rate[i],
                                           is_training=self.is_training,
                                           mr_fms=self.mr_path_fms[i],
                                           activation=self.act_hidden,
                                           reuse=self.reuse,
                                           padding=self.padding,
                                           kernel_regularizer=
                                           self.kernel_regularizer,
                                           bias_regularizer=
                                           self.bias_regularizer)
        output_tensor, self.logit_tensor = output_layer(
            h, self.channels_out, self.act_out, data_format=self.data_format,
            reuse=self.reuse, kernel_regularizer=self.kernel_regularizer,
            bias_regularizer=self.bias_regularizer)
        self._set_output(output_tensor)
        self.calculate_border_size()
        print('Output shape: {}'. format(output_tensor.get_shape().as_list()))
        print('Total number of parameters: {}'.format(self._num_params()))
        return self.output_tensor

    def build_model(self, inputs, labels=None, weights=None):
        """
        Create the computational graph.
        Args:
            inputs: list or tensor
                Inputs tensor or list of input size. In the latter case a
                placeholder for the labels is generated.
                (see also create_pl_tensor).
            labels:  tensor
                Label tensor. If none then a placeholder with the correct shape
                will be created
            weights: tensor
                Label weight tensor.
        """

        self.inference(inputs)
        if weights is not None:
            self.weight_tensor = weights

        if labels is not None:
            self.label_tensor = labels
        else:
            self.create_pl_label_tensor()
        self.loss_tensor = self.loss()

    def model(self, inputs, labels=None, weights=None):
        # compatibility
        self.build_model(inputs, labels, weights)

    def build_inference_model(self, inputs=None):
        return self.inference(inputs)

    def calculate_border_size(self, inputs=None):
        if self.padding == "same":
            self.border_size = np.zeros((3,))
        else:
            self.border_size = self.num_layer * np.asarray([10, 10, 4])
        return self.border_size

    def calculate_border_brute_force(self, inputs=None):
        return self.calculate_border_size()

    def add_regularizer(self, name: str, **kwargs):
        if name == 'rec':
            print('Adding reconstruction regularizer.')
            opts = {'weight': 0.05, 'data_format': self.data_format}
            if kwargs is not None:
                opts.update(kwargs)
            reg_t = tf.get_default_graph().get_tensor_by_name(
                "layer_{}/reduce/mul_1:0".format(self.num_layer-1))
            self.loss_tensor = regularizer.reconstruction_reg(
                    self.input_tensor, reg_t, self.loss_tensor, **opts)

    @staticmethod
    def from_config(config_file, construct_graph=True, load_ckpt=True,
                    config_dict=None, **kwargs):
        from pprint import pprint
        with open(config_file) as f:
            tmp = f.read()
            idx = tmp.find("if __name__ == \"__main__\"")
            tmp = tmp[:idx]
        if config_dict is None:
            config_dict = dict()
        exec(tmp, {}, config_dict)
        config_dict.update(kwargs)

        # input arguments to the constructor
        # TODO: this should be somehow automated so this does not need to be
        # TODO: udpated if the constructor changes
        keys = ('num_layer', 'channels_out', 'loss', 'act_hidden', 'act_out',
                'data_format', 'dropout_rate', 'feature_maps', 'mr_path_fms',
                'padding', 'kernel_regularizer', 'bias_regularizer')

        # fix some inconsistencies in config files
        if 'num_lyr' in config_dict:
            config_dict['num_layer'] = config_dict['num_lyr']
        if 'channels_out' not in config_dict:
            config_dict['channels_out'] = 1

        conf_keys = config_dict.keys()
        keys = set(keys).intersection(set(conf_keys))
        cnet_dict = {key: config_dict[key] for key in keys}
        print("Building network from config file {} with settings:."
              .format(config_file))
        pprint(cnet_dict)
        cnet = DefaultMRNet(**cnet_dict)
        if construct_graph:
            cnet.build_inference_model(config_dict['input_shape'])
        if load_ckpt:
            if 'ckpt_file' in config_dict:
                ckpt_file = config_dict['ckpt_file']
            else:
                ckpt_file = cnet.get_latest_ckpt(config_dict['output_dir'])
                config_dict['ckpt_file'] = ckpt_file
            cnet.load_from_ckpt(ckpt_file)
        return cnet
