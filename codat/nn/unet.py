""" 3d U-net like network.

@author: Benedikt Staffler <benedikt.staffler@brain.mpg.de

"""

import tensorflow as tf
import codat.nn.util
import numpy as np
from codat.nn.fcn import FCN
from typing import List, Tuple


class UNet(FCN):
    def __init__(self,
                 feature_maps,
                 kernel_size=None,
                 kernel_size_exp=None,
                 pool_size=None,
                 act_hidden=None,
                 act_out=None,
                 loss='squared',
                 num_classes: int = 1,
                 dropout_rate: List[float] = None,
                 data_format='channels_last',
                 padding=None,
                 kernel_regularizer=None,
                 args = {},
                 bias_regularizer=None):
        """
        3D u-net implementation.
        
        Args:
            feature_maps: Feature maps for each resolution.
            kernel_size: Convolutional kernel sizes for the contractive path,
                such that kernel_size[res][i] specifies the i-th convolutional
                kernel at resolution res.
            kernel_size_exp: Convolutional kernel sizes for the expansive path.
                kernel_size_exp[res][i] specifies the i-th kernel at resolution
                res. Note that kernel_size_exp must have length(kernel_size) - 1
                since the lowest resolution is not considered as part of the
                contractive path.
            pool_size: List of pooling sizes that is used for downsampling of
                the corresponding resolution, i.e. pool_size[res] specifies the
                downsampling for resolution res. The upsampling in the
                contractive path is done with the same pool_size as the
                corresponding downsampling operation.
            act_hidden: Hidden activation (Default: codat.nn.util.tanh_scaled)
            act_out: Output activatio (Default: codat.nn.util.tanh_scaled for
                'squared loss' and tf.nn.softmax for 'cross-entropy' loss.
            loss: 'squared' or 'cross-entropy'
            num_classes: Number of output channels.
            dropout_rate: List of dropout rates of length
                (2*len(feature_map) - 1) specifying the dropout rate which is
                applied before the multi-convolution step in each resolution
                level. The first len(feature_map) dropout rates correspond to
                the contractive path und the rest to the expansive path.
            data_format: 'channels_last' (Default) or 'channels_first'
            padding: Padding for convolutional layer.
                (Default: "valid")
        """

        FCN.__init__(self)
        self.num_classes = num_classes
        self.input_tensor = None
        self.input_shape = None
        self.output_tensor = None
        self.output_shape = None
        self.data_format = data_format
        self.contractive_path = []
        self.expansive_path = []
        self.weight_tensor = None
        self.loss_tensor = None
        self.label_tensor = None
        self.feature_maps = feature_maps
        self.loss_name = loss
        self.is_training = True
        if padding is None:
            self.padding = "valid"
        else:
            self.padding = padding

        # kernel size (default is two successive 3x3x3 convs in each layer)
        if kernel_size is None:
            self.kernel_size = [[[3, 3, 3], [3, 3, 1]]
                                for _ in range(len(feature_maps))]
        else:
            self.kernel_size = kernel_size
        assert len(self.kernel_size) == len(feature_maps),\
            "Kernel size must be specified for all resolutions."

        # kernel size for expansive path
        # (default is the same as for the contractive path)
        if kernel_size_exp is None:
            self.kernel_size_exp = self.kernel_size[:-1]
        else:
            if len(kernel_size_exp) == len(feature_maps):
                print('Warning: ''kernel_size_exp'' was specified in legacy '
                      'mode and the last entry will be removed.')
                self.kernel_size_exp = kernel_size_exp[:-1]
            else:
                self.kernel_size_exp = kernel_size_exp
        assert len(self.kernel_size_exp) == len(feature_maps) - 1, \
            "kernel_size_exp must be specified for all but the lowest" \
            "resolution."

        # pool size (default is 2x2x1 in first layer and 2x2x2 in other layers)
        if pool_size is None:
            self.pool_size = [[2, 2, 1], ] + \
                             [[2, 2, 2] for _ in range(len(feature_maps) - 2)]
        else:
            self.pool_size = pool_size

        # non-linearities
        if act_hidden is None:
            self.act_hidden = codat.nn.util.tanh_scaled
        else:
            self.act_hidden = act_hidden

        # linear option due to legacy handling of none input
        if act_out is None:
            if self.loss_name == 'squared':
                act_out = codat.nn.util.tanh_scaled
            elif self.loss_name == 'cross-entropy':
                act_out = tf.nn.softmax
            elif self.loss_name == 'cross-entropy-reg':
                act_out = tf.nn.softmax
            elif self.loss_name == 'focal-loss':
                act_out = None ##define later
            elif self.loss_name == 'dice':
                act_out = tf.nn.softmax
            else:
                raise RuntimeError('Specify act_out.')
        elif act_out == "linear":
            act_out = None
        self.act_out = act_out
        
        if self.loss_name == 'focal-loss':
            self.args = args
        
        # DROPOUT
        if dropout_rate is None:
            self.dropout_rate = [0] * (2 * len(feature_maps) - 1)
        elif type(dropout_rate) is not list:
            raise RuntimeError("Dropout rate must be specified as a list.")
        elif len(dropout_rate) == 1:
            self.dropout_rate = dropout_rate * (2 * len(feature_maps) - 1)
        elif len(dropout_rate) == len(feature_maps):
            print('Dropout is specified in legacy mode.')
            # just a hack to make the old specifications valid
            self.dropout_rate = [0] * (2 * len(feature_maps) - 1)
            self.dropout_rate[:len(feature_maps)] = dropout_rate
        else:
            assert len(dropout_rate) == len(feature_maps),\
                'Wrong dropout specified.'
            self.dropout_rate = dropout_rate
        self.kernel_regularizer = kernel_regularizer
        self.bias_regularizer = bias_regularizer
        
    def build_model(self,
                    inputs: tf.Tensor,
                    labels: tf.Tensor = None,
                    weights: tf.Tensor = None):
        """
        Create the unet model specified in inference.
        Sets the corresponding tensor members of the class.

        Args:
            inputs: input tensor
                If a list of integers is supplied then a placeholder is created.
            labels: label/target tensor
                If a list of integers is supplied then a placeholder is created.
            weights: weight tensor for the weighted loss function
                If none the all weights are 1
        """
        self.inference(inputs)
        if weights is not None:
            self.weight_tensor = weights
        if labels is None:
            self.create_pl_label_tensor()
        else:
            self.label_tensor = self.create_pl_tensor(labels)
        self.loss_tensor = self.loss()

    def build_inference_model(self, inputs=None):
        return self.inference(inputs)

    def inference(self,
                  inputs: tf.Tensor) -> tf.Tensor:
        """
        Quick and dirty u-net like inference model for anisotropic 3d data
        (2.5 anisotropy in z).
        Model was tested for shape (108, 108, 52)
        Args:
            inputs: input tensor
                If a list of integers is supplied then a placeholder is created.

        Returns:
            out: The inference model output tensor.
        """

        self._set_input(inputs)

        fm = self.feature_maps
        t = self.input_tensor
        print('Input shape: {} (data format: {})'
              .format(t.get_shape().as_list(), self.data_format))

        # contractive path
        self.contractive_path = []
        for lyr in range(len(fm)):
            t = self._multi_conv(t, fm[lyr], self.kernel_size[lyr],
                                 activation=self.act_hidden,
                                 name='res_{}_contractive'.format(lyr),
                                 dropout_rate=self.dropout_rate[lyr],
                                 is_training=self.is_training)
            self.contractive_path.append(t)
            pshp = self.pool_size[lyr] if lyr < len(fm) - 1 else '-'
            this_drop_rate = self.dropout_rate[lyr] if self.is_training else 0
            print('Contractive path res_{} shape: {} (conv shapes: {}, '
                    'pool shape: {}, dropout rate: {})'
                  .format(lyr, self.contractive_path[-1].get_shape().as_list(),
                          self.kernel_size[lyr], pshp, this_drop_rate))
            if lyr < (len(fm) - 1):  # no downsampling in lowest res
                curShp = self.get_spatial_shape(t)
                curMP = self.pool_size[lyr]
                if not np.all((curShp % curMP) == 0):
                    print('Warning: Tensor shape {} not divisible by max '
                          'pooling shape {}'.format(curShp, curMP))
                t = self._downsample(t, strides=self.pool_size[lyr],
                                     name='res_{}_to_res_{}'.format(lyr, lyr+1))

        # expansive path
        self.expansive_path = [None] * len(fm)
        self.expansive_path[len(fm) - 1] = t
        for i, lyr in enumerate(reversed(range(1, len(fm)))):
            t = self._up_conv_merge(self.contractive_path[lyr - 1],
                                    self.expansive_path[lyr],
                                    'res_{}_to_res_{}'.format(lyr, lyr-1),
                                    activation=self.act_hidden,
                                    pool_shape=self.pool_size[lyr-1])
            t = self._multi_conv(t, fm[lyr-1], self.kernel_size_exp[lyr-1],
                                 'res_{}_expansive'.format(lyr - 1),
                                 activation=self.act_hidden,
                                 dropout_rate=self.dropout_rate[i + len(fm)],
                                 is_training=self.is_training)
            self.expansive_path[lyr-1] = t
            this_drop_rate = self.dropout_rate[i + len(fm)] if \
                self.is_training else 0
            print('Expansive path res_{} and res_{}: {} (conv shapes:{}, '
                  'dropout rate: {})'
                  .format(lyr, lyr - 1,
                          self.expansive_path[lyr-1].get_shape().as_list(),
                          self.kernel_size_exp[lyr-1], this_drop_rate))

        # output
        logits = self._multi_conv(t, self.num_classes, [[1, 1, 1], ],
                                  'output/logits',
                                  activation=None)
        self.logit_tensor = logits

        with tf.variable_scope("output/pred"):
            if self.act_out is not None:
                out = self.act_out(logits)
            else:
                out = logits

        self._set_output(out)
        self.calculate_border_size()
        print('Output shape: {}'.format(out.get_shape().as_list()))
        print('Total number of parameters: {}'.format(self._num_params()))
        return out

    def _multi_conv(self,
                    t: tf.Tensor,
                    feature_maps: int,
                    kernel_size: List[List[int]],
                    name: str,
                    activation=codat.nn.util.tanh_scaled,
                    dropout_rate=None,
                    is_training=True) -> tf.Tensor:
        """
        Multiple consecutive convolutions all with the same numer of feature
        maps and kernel size.
        Args:
            t: input tensor
            feature_maps: number of feature maps
            kernel_size: kernel size as a list of lists for each convolution
                (e.g. [[3, 3, 3], [3, 3, 3]] for default unet)

        Returns:
            t: output tensor
        """

        init_w = tf.contrib.layers.xavier_initializer()
        if dropout_rate is not None and dropout_rate > 0:
            t = tf.layers.dropout(t, dropout_rate, training=is_training,
                                  name=name + '/dropout')
        for i in range(len(kernel_size)):
            t = tf.layers.conv3d(t, feature_maps, kernel_size[i],
                                 activation=activation,
                                 kernel_initializer=init_w,
                                 name=name + '/conv_{}'.format(i),
                                 padding=self.padding,
                                 data_format=self.data_format,
                                 kernel_regularizer=self.kernel_regularizer,
                                 bias_regularizer=self.bias_regularizer)
        return t

    def _downsample(self,
                    t: tf.Tensor,
                    strides=2,
                    name: str = None) -> tf.Tensor:
        t = tf.layers.max_pooling3d(t, strides, strides=strides,
                                    padding=self.padding,
                                    data_format=self.data_format,
                                    name=name + '/max_pool')
        return t

    def _upsample(self,
                  t: tf.Tensor,
                  feature_maps: int,
                  strides: Tuple[int, ...] = (2, 2, 2),
                  kernel_size=(2, 2, 2),
                  name: str = None,
                  activation=codat.nn.util.tanh_scaled) -> tf.Tensor:
        init_w = tf.contrib.layers.xavier_initializer()
        t = tf.layers.conv3d_transpose(t, feature_maps, kernel_size,
                                       strides=strides,
                                       padding=self.padding,
                                       data_format=self.data_format,
                                       activation=activation,
                                       kernel_initializer=init_w,
                                       name=name + '/upconv')
        return t

    def _crop(self,
              t: tf.Tensor,
              out_shape: List[int]) -> tf.Tensor:
        ts = t.get_shape().as_list()
        if self.data_format == 'channels_last':
            begin = [x - y for x, y in zip(ts[1:-1], out_shape[1:-1])]
        else:
            begin = [x - y for x, y in zip(ts[2:], out_shape[2:])]
        assert all(map(lambda x: x % 2 == 0, begin))
        begin = list(map(lambda x: x // 2, begin))  # crop symmetrically
        if self.data_format == 'channels_last':
            begin = [0] + begin + [0]
            t = tf.slice(t, begin, out_shape)
        else:
            begin = [0, 0] + begin
            t = tf.slice(t, begin, out_shape)
        return t

    def _up_conv_merge(self,
                       t1: tf.Tensor,
                       t2: tf.Tensor,
                       name: str = None,
                       pool_shape=(2, 2, 2),
                       activation=codat.nn.util.tanh_scaled) -> tf.Tensor:
        s1 = t1.get_shape().as_list()
        if self.data_format == 'channels_last':
            num_c = s1[-1]
        else:
            num_c = s1[1]
        t2 = self._upsample(t2, num_c, strides=pool_shape,
                            kernel_size=pool_shape,
                            name=name,
                            activation=activation)
        with tf.variable_scope(name + "/concat"):
            t1 = self._crop(t1, t2.get_shape().as_list())

            if self.data_format == 'channels_last':
                t = tf.concat([t1, t2], axis=4)
            else:
                t = tf.concat([t1, t2], axis=1)
            return t
    
    @staticmethod
    def from_config(config_file, construct_graph=True, load_ckpt=True,
                    config_dict=None, **kwargs):
        from pprint import pprint
        with open(config_file) as f:
            tmp = f.read()
            idx = tmp.find("if __name__ == \"__main__\"")
            tmp = tmp[:idx]
        if config_dict is None:
            config_dict = dict()
        exec(tmp, {}, config_dict)
        config_dict.update(kwargs)

        # input arguments to the constructor
        # TODO: this should be somehow automated so this does not need to be
        # TODO: udpated if the constructor changes
        keys = ('feature_maps', 'kernel_size', 'kernel_size_exp', 'pool_size',
                'act_hidden', 'loss', 'num_classes', 'dropout_rate', 'padding',
                'kernel_regularizer', 'bias_regularizer')

        conf_keys = config_dict.keys()
        keys = set(keys).intersection(set(conf_keys))
        cnet_dict = {key: config_dict[key] for key in keys}
        print("Building network from config file {} with settings:."
              .format(config_file))
        pprint(cnet_dict)
        cnet = UNet(**cnet_dict)
        if construct_graph:
            cnet.build_inference_model(config_dict['input_shape'])
        if load_ckpt:
            if 'ckpt_file' in config_dict:
                ckpt_file = config_dict['ckpt_file']
            else:
                ckpt_file = cnet.get_latest_ckpt(config_dict['output_dir'])
                config_dict['ckpt_file'] = ckpt_file
            cnet.load_from_ckpt(ckpt_file)
        return cnet

    def shapes_from_low_res(self,
                            low_res_shape: List[int]):
        """
        Calculate the size of the network input and output for a given shape
        in the lowest resolution.
        Args:
            low_res_shape: spatial shape in the lowest resolution after
                convolution
            verbose: Print detailed information about shapes

        Returns:
            input_shape: spatial shape of the input the results in low_res_shape
            output_shape: spatial shape of the output resulting from
            low_res_shape
        """

        shp = np.asarray(low_res_shape)
        in_shp = shp
        if self.padding == "same":
            in_shp = shp * np.prod(np.asarray(self.pool_size), axis=0)
            out_shp = in_shp
        else:
            for res in reversed(range(len(self.feature_maps))):
                for i in range(len(self.kernel_size[res])):
                    in_shp = in_shp + np.asarray(self.kernel_size[res][i]) - 1
                if res > 0:
                    in_shp = in_shp * np.asarray(self.pool_size[res-1])
            out_shp = shp
            for res in reversed(range(1, len(self.feature_maps))):
                out_shp = out_shp * np.asarray(self.pool_size[res-1])
                for i in range(len(self.kernel_size_exp[res-1])):
                    out_shp = out_shp - \
                              (np.asarray(self.kernel_size_exp[res-1][i]) - 1)
        return in_shp, out_shp

    def calculate_border_size(self, inputs=None):
        if self.padding == "same":
            self.border_size = np.zeros((3,))
        elif inputs is not None:
            super(UNet, self).calculate_border_size(inputs)
        else:
            # choose large low_res size
            shp = np.sum(np.sum(np.asarray(self.kernel_size_exp), axis=0),
                         axis=0)
            shp_in, shp_out = self.shapes_from_low_res(shp)
            self.border_size = shp_in - shp_out

    def calculate_border_brute_force(self, inputs=None):
        self.calculate_border_size()
