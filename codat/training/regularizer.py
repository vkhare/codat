""" Code for network training regularizer.

Created on 18.09.18 15:46

@author: Benedkt Staffler <benedikt.staffler@brain.mpg.de>

"""

import tensorflow as tf
from tensorflow.python.framework import ops
from tensorflow.python.platform import tf_logging as logging
from typing import List
import numbers


def reconstruction_reg(in_t, reg_t, loss_t, weight,
                       gauss_mean=None,
                       gauss_std=None,
                       data_format='channels_last'):
    """
    Adds an additional term to the loss function corresponding to the input data
    (optionally corrupted by noise).
    
    Note: This function is currently supposed to be called after graph
          construction (i.e. after fcn.build_model) and is thus directly added
          to the specified input loss (and not via the REGULARIZATION_LOSSES
          graph keys).
    
    see also Sabour, Frosst, Hinton, Dynamic Routing Between Capsules, 2017
    
    Args:
        in_t: The input tensor to the network.
        reg_t: The tensor from which the input is reconstructed (typically the
            last hidden layer in a network).
        loss_t: The loss tensor for training.
        weight: The weight of the regularizer term in the loss function as
            tensor or float.
        gauss_mean, gauss_std: Mean and standard deviation for noise that is
            added to the input tensor.
        data_format: 'channels_last' or 'channels_first'
    
    Returns:
        loss_t: The new loss tensor consisting of the input loss tensor plus the
        regularization loss.
        
    """

    shp_in = in_t.get_shape().as_list()
    shp_out = reg_t.get_shape().as_list()
    shape_crop = shp_out.copy()
    if data_format == 'channels_last':
        c_in = shp_in[-1]
        shape_crop[-1] = c_in
    else:
        c_in = shp_in[1]
        shape_crop[1] = c_in

    with tf.variable_scope('loss/reconstruction_regularizer'):
        with tf.variable_scope('crop_input_to_output_shape'):
            in_cropped_t = _crop(in_t, shape_crop)
        in_c_im = tf.slice(in_cropped_t, [0, 0, 0, 0, 0],
                          [1, shp_out[1], shp_out[2], 1, 1])
        in_c_im = tf.squeeze(in_c_im, 4)
        tf.summary.image('rec_in/', in_c_im)

        # use simple 1x1x1 conv layer with linear activation for now
        rec_t = tf.layers.conv3d(reg_t, c_in, (1, 1, 1),
                                 data_format=data_format)

        # add noise to target if specified
        if gauss_mean is not None and gauss_std is not None:
            noise = tf.random_normal(shape=tf.shape(in_cropped_t),
                                     mean=gauss_mean, stddev=gauss_std,
                                     dtype=tf.float32)
            in_cropped_t = tf.add(in_cropped_t, noise, 'add_noise')
        reg = tf.losses.mean_squared_error(in_cropped_t, rec_t)
        reg = tf.multiply(weight, reg, name='mul_reg_weight')
        ops.add_to_collection(ops.GraphKeys.REGULARIZATION_LOSSES, reg)
        loss_t = tf.add(loss_t, reg, name='add_rec_reg')
        tf.summary.scalar('loss/rec_reg', reg)

        rec_im = tf.slice(rec_t, [0, 0, 0, 0, 0],
                          [1, shp_out[1], shp_out[2], 1, 1])
        rec_im = tf.squeeze(rec_im, 4)
        tf.summary.image('rec/', rec_im)

    return loss_t


def orthogonal_regularizer(scale, scope=None):
    if isinstance(scale, numbers.Integral):
        raise ValueError('scale cannot be an integer: %s' % (scale,))
    if isinstance(scale, numbers.Real):
        if scale < 0.:
            raise ValueError(
                'Setting a scale less than 0 on a regularizer: %g.' %
                scale)
        if scale == 0.:
            logging.info('Scale of 0 disables regularizer.')
            return lambda _: None

    def ortho_loss(weights):
        """
        Applies orthogonal regularization to weights.
        The regularizer if of the format
        \|W^T * W - 1\|²,
        where W is a NxM matrix where M corresponds to the last dimension of the
        input weights and N contains all other dimensions and the norm is the
        Frobenius norm.
        
        see also Brock et al., 2017, ICLR (https://arxiv.org/abs/1609.07093)
            or Xie, 2017, arXiv (https://arxiv.org/pdf/1703.01827.pdf)
        
        """
        with ops.name_scope(scope, 'ortho_regularizer', [weights]) as name:
            shp = weights.get_shape().as_list()
            my_scale = ops.convert_to_tensor(scale,
                                             dtype=weights.dtype.base_dtype,
                                             name='scale')
            w = tf.reshape(weights, shape=(-1, shp[-1]))
            # rescale columns to have norm 1?
            # w = tf.divide(w, tf.norm(w, axis=0, keepdims=True))
            loss = tf.subtract(tf.matmul(tf.transpose(w), w), tf.eye(shp[-1]))
            loss = tf.nn.l2_loss(loss)
            return tf.multiply(my_scale, loss, name=name)

    return ortho_loss


def l1_l2_ortho_regularizer(scale_l1=1.0, scale_l2=1.0, scale_orth=1.0,
                            scope=None):
        """Returns a function that can be used to apply L1 L2 regularizations.
        Args:
          scale_l1: A scalar multiplier `Tensor` for L1 regularization.
          scale_l2: A scalar multiplier `Tensor` for L2 regularization.
          scale_orth: A scalar multipler `Tensor` for orthogonal regularization.
          scope: An optional scope name.
        Returns:
          A function with signature `func(weights)` that applies a weighted
          sum of L1, L2 and orthogonal regularization.
        Raises:
          ValueError: If scale is negative or if scale is not a float.
        """

        if isinstance(scale_l1, numbers.Integral):
            raise ValueError('scale_l1 cannot be an integer: %s' % (scale_l1,))
        if isinstance(scale_l2, numbers.Integral):
            raise ValueError('scale_l2 cannot be an integer: %s' % (scale_l2,))
        scope = scope or 'l1_l2_ortho_regularizer'
        return tf.contrib.layers.sum_regularizer(
            [tf.contrib.layers.l1_regularizer(scale_l1),
             tf.contrib.layers.l2_regularizer(scale_l2),
             orthogonal_regularizer(scale_orth)],
            scope=scope)


def _crop(t: tf.Tensor,
          out_shape: List[int],
          data_format='channels_last') -> tf.Tensor:
    ts = t.get_shape().as_list()
    if data_format == 'channels_last':
        begin = [x - y for x, y in zip(ts[1:-1], out_shape[1:-1])]
    else:
        begin = [x - y for x, y in zip(ts[2:], out_shape[2:])]
    assert all(map(lambda x: x % 2 == 0, begin))
    begin = list(map(lambda x: x // 2, begin))  # crop symmetrically
    if data_format == 'channels_last':
        begin = [0] + begin + [0]
        t = tf.slice(t, begin, out_shape)
    else:
        begin = [0, 0] + begin
        t = tf.slice(t, begin, out_shape)
    return t
