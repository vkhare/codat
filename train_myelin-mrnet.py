"""
@author: @author: Benedikt Staffler <benedikt.staffler@brain.mpg.de

"""


import tensorflow as tf
import codat.nn.mrNet as net
import codat.data.segem
import codat.training.util
import codat.training.get_empty_gpu
import os
import numpy as np
import wkw
from codat.training.trainer import train_model_mts as train_model
import argparse 

# input_shape = np.asarray([1, 108, 108, 40, 2], dtype=np.uint32)

weight_myelin = 0.9
num_classes = 3
data_format = 'channels_last'
feature_set = []

feature_maps = [16]
pool_size = None
act_out = None
input_shape = (1,108, 108, 48,2)
num_layer = 7
mean = 143.0835021480915
std = 47.77946098242964 * 0.99
global_args = None
# training data
DATA = '/tmpscratch/webknossos/Connectomics_Department/2018-11-13_scMS109_1to7199_v01_l4_06_24_fixed_mag2/'
RAW_DATA_PATH = DATA+"/color/2-2-1/"
TARGET_DATA_PATH = "/tmpscratch/webknossos/Connectomics_Department/2018-11-13_scMS109_1to7199_v01_mag2_varun_myelin_training_annotations/segmentation/1/"
SEG_PATH = DATA+'segmentation/2-2-1/'
TRAIN_MAT = "../myelin_seg_data/train.mat"
# output paths
output_dir = "../tmp/unet/"
overwrite = True
add_noise = None
mult_noise = None
# training options
max_steps = 1e7
max_runtime = 14 * 24 * 3600  # 14d
add_image_summaries = True
add_weight_summaries = True
# optimizer = tf.train.MomentumOptimizer
optimizer = tf.train.AdamOptimizer
learning_rate = 1e-6
lr_decay_rate = 0.98  # per 10000 steps
optimizer_args = {'beta1':0.9,'beta2':0.999}
# optimizer_args = {'momentum': 0.9}

# logging
LOG_FREQUENCY = 1
# load from checkpoint file
CKPT_FILE = None

# summaries and checkpoints
SAVE_CKPT_SECS = 900
SAVE_SUMM_STEPS = 1
LOG_COUNT_STEPS = 1
args_focal ={}
def get_cnn():
    return net.DefaultMRNet(num_layer=num_layer,
                 channels_out=3,
                 loss=global_args.loss,
                 act_out=act_out,
                 data_format=data_format,
                 feature_maps=feature_maps,
                 mr_path_fms = ([16, 16, 16],))


def train(raw_data_path=RAW_DATA_PATH, target_data_path= TARGET_DATA_PATH, seg_path = SEG_PATH,
          train_file=TRAIN_MAT,
          ckpt_file=CKPT_FILE):

    # create output dir and copy config
    codat.training.util.create_output_folder(output_dir, overwrite=overwrite)
    codat.training.util.copy_config_to(os.path.realpath(__file__), output_dir)

    # create cnet
    cnet = get_cnn()
    cnet.calculate_border_brute_force(input_shape)

    # training data
#     with tf.device('/gpu:1'):
    data = codat.data.segem.myelin_segem_input(raw_data_path,target_data_path,seg_path,train_file,
            input_shape[1:-1], input_shape[1:-1] - cnet.border_size,add_noise=add_noise,mult_noise=mult_noise,
            flip=global_args.flip,rotate=global_args.rotate,border_diff = cnet.border_size,
            weight_myelin=weight_myelin, num_calls=global_args.num_calls,
            batch_size=global_args.batch,step_drop=global_args.step_drop, data_format=data_format,
            sample=global_args.sampling,seg=global_args.add_seg,count=global_args.start_sample_count)

    cnet.build_model(*data)

    # training
    train_model(cnet,
                mean= mean,std=std,
                optimizer=optimizer,
                optimizer_args=optimizer_args,
                learning_rate=learning_rate,
                lr_decay_rate=lr_decay_rate,
                add_weight_summaries=add_weight_summaries,
                add_image_summaries=add_image_summaries,
                log_frequency=LOG_FREQUENCY,
                max_steps=max_steps,
                max_runtime=max_runtime,
                output_dir=output_dir,
                save_ckpt_secs=SAVE_CKPT_SECS,
                save_summ_steps=SAVE_SUMM_STEPS,
                log_count_steps=LOG_COUNT_STEPS,
                ckpt_file=ckpt_file,
                max_to_keep=global_args.max_to_keep)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Add batch_size and other configs')
    parser.add_argument('--batch',default=1,type=int,
                    help='Batch size (default: 1)')
    parser.add_argument('--lr_decay_rate',default=0.98,type=float,
                    help='lr decay rate (default: 0.98)')
    parser.add_argument('--add_noise',default=0.98,type=float,
                    help='additiove noise (default:None)')
    parser.add_argument('--mult_noise',default=0.98,type=float,
                    help='multiplicative noise(default: None)')
    parser.add_argument('--step_drop',default=10000,type=int,
                    help='Steps after which myelin sampling decay. Usually set to 10k/batch_size (default: 10000)')
    parser.add_argument('--output_dir',default='/tmpscratch/vkhare/models/mrnet',type=str,
                    help='tensor board log dir (default: /tmpscratch/models/mrnet)')
    parser.add_argument('--start_sample_count',default=1,type=int,
                    help='continue training enter last trained step 1 if no training done (default: 1)')
    parser.add_argument('--max_steps',default=9e5,type=int,
                    help='Steps after which lr and myelin sampling decay. Usually set to 9e5/batch_size (default: 900,000)')
    parser.add_argument('--lr',default=1e-3,type=float,
                    help='learning rate (default: 1e-3)')
    parser.add_argument('--save_every',default=4*3600,type=int,
                    help='save a checkpoint after every \'save_every\' seconds (default: 4 hrs = 4*3600)')
    parser.add_argument('--max_to_keep',default=5,type=int,
                    help='maximum checkpoints stored (default: 5)')
    parser.add_argument('--num_calls',default=10,type=int,
                    help='number of parallel cpu cores at batching (default: 10)')
    parser.add_argument('--sampling',default='lin',type=str,
                    help='sampling strategy (default: linear decay')
    parser.add_argument('--loss',default='cross-entropy',type=str,
                    help='loss (default: cross-entropy')
    parser.add_argument('--gamma',default=2,type=float,
                    help='gamma parameter for focal loss')
    parser.add_argument('--beta',default=1,type=float,
                    help='beta parameter for focal loss')
    parser.add_argument('--add_seg',default=0,type=int,
                    help='add cell segmentation mask default 0')
    parser.add_argument('--augment',dest='augment', action='store_true',
                    help='add noise augmentation')
    parser.add_argument('--flip', default=False, action='store_true',
                        help='add rotate augmentation')
    parser.add_argument('--rotate', default=False, action='store_true',
                        help='add rotate augmentation')
    parser.add_argument('--append',default='',type=str,
                    help='append to directory name')
    args = parser.parse_args()
    global_args = args
    max_steps = args.max_steps
    args_focal = {'gamma':args.gamma,'beta':args.beta}
    learning_rate = args.lr
    lr_decay_rate = args.lr_decay_rate
    SAVE_CKPT_SECS = args.save_every
    output_dir = (args.output_dir+'_'+args.loss 
                +'_'+str(args.batch)+'_'+str(SAVE_CKPT_SECS) 
                +'_'+args.sampling+'_'+str(learning_rate)+"_augment-"+str(args.augment) 
                +'_add_noise-'+str(args.add_noise)+"_mult_noise-"+str(args.mult_noise)
                +'_flip-'+str(args.flip)+'_rotate-'+str(args.rotate)
                +'_seg_'+str(args.add_seg)+'_'+args.append)
    CKPT_FILE = output_dir
    if args.augment:
        add_noise = {'mean': 0, 'std': args.add_noise, 'std_mean': 0}
        mult_noise = {'mean': 1, 'std': args.mult_noise, 'mean_range': 0}
#     if global_args.add_seg:
# #         input_shape = np.asarray([1, 108, 108, 40, 2], dtype=np.uint32)
#         input_shape = np.asarray([1,444, 444, 104,2], dtype=np.uint32)
# #         input_shape = np.asarray([1,252, 252, 56,2], dtype=np.uint32)
    print('Training cnet with BATCH_SIZE ',args.batch," and output logs in ",output_dir)
    codat.training.get_empty_gpu.get_empty_gpu()
    train()