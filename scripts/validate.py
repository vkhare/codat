import sys

sys.path.append('/u/vkhare/codat/')
import tensorflow as tf
import numpy as np
import codat.nn.unet
import codat.nn.deep_labv3 as deepnet
import wkw
import tqdm
import glob
import codat.training.get_empty_gpu
from collections import defaultdict
import scipy.io as sio
import os
from skimage import transform
from evaluation_utils.architectures import Architectures
from evaluation_utils.evaluate import Evaluate,PredHolder

codat.training.get_empty_gpu.get_empty_gpu()
#parser = argparse.ArgumentParser(description='Add model dir and other configs')
#parser.add_argument('--model_dir',default='',type=str,
#                    help='tensor board log dir (default: /tmpscratch/models/unet)')
#parser.add_argument('--output_dir',default='/tmpscratch/vkhare/models/unet',type=str,
#                    help='tensor board log dir (default: /tmpscratch/models/unet)')


data_format = 'channels_last'

DATA = '/tmpscratch/georgwie/artifacts/default/merge_mappings/mhlab_ds2__unet_d3_f16_b4_volume_50__column_06_24__segmentation__globalized_64__rag__agglomeration__merged_mappings/wkw_with_merged_mappings/'
#'../myelin_seg_data/seg_dst_mask' #preloaded dst mask for speed
nodes = sio.loadmat('../myelin_seg_data/train.mat')
cubes_in = nodes['final'].T
num_Classes=3
ckpt_file = '/tmpscratch/vkhare/models/'+sys.argv[1]

files = sorted(glob.glob(os.path.join(ckpt_file, 'model*.data-*')),
               key=os.path.getctime)
out = [os.path.splitext(file)[0] for file in files]

cubes_val_idx = [3,4,11,27,16]
PR = defaultdict(list)
files = sorted(glob.glob(os.path.join(ckpt_file, 'model*.data-*')),
               key=os.path.getctime)
out = [os.path.splitext(file)[0] for file in files]
architecture = Architectures(type=int(sys.argv[2]),res=int(sys.argv[3]),ckpt=ckpt_file,DATA = DATA)
for step,model in enumerate(out[-10:]):
    architecture.initiate_model(model)
    for i in cubes_val_idx:
        coor = np.min(cubes_in[i,:,:],axis=1)[:3].astype(np.int64)-1
        pred_soft = architecture.predict(coor)
        tc = architecture.read_tc(coor)
        PR[model].append(PredHolder(tc,pred_soft,i))

Evaluate(3,PR)




