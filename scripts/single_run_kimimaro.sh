#!/bin/bash
source ~/.bashrc
#### Standard values of Variables
# FLAGS
SLURM_ARRAY_TASK_ID=${SLURM_ARRAY_TASK_ID:-}
JOB_ID=${JOB_ID:-${SLURM_ARRAY_JOB_ID}}
JOB_NAME=${JOB_NAME:-${SLURM_JOB_NAME}}

# Directories
py_script_path=${py_script_path:-"/u/vkhare/codat"}

LD_LIBRARY_PATH=/u/vkhare/conda-envs/mpcdf_py3.7/lib/:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH
echo "the ld library path is $LD_LIBRARY_PATH"

if (([ $(hostname) != "gaba" ] && [ $(hostname) != "gaba01" ]) && [ $(hostname) != "gaba02" ])
then
	export SGE_CELL="GABA"
	export MODULEPATH=${MODULEPATH}:/afs/ipp/common/usr/modules.2017/@sys/modulefiles/${SGE_CELL}/applications:/afs/ipp/common/usr/modules.2017/@sys/modulefiles/${SGE_CELL}/libs:/afs/ipp/common/usr/modules.2017/@sys/modulefiles/${SGE_CELL}/tools:/afs/ipp/common/usr/modules.2017/@sys/modulefiles/${SGE_CELL}/GPU
	echo 'Modules are available now'
fi


# cd to script path
cd $py_script_path
module load anaconda/3/5.1.0
source activate mpcdf_py37
# Set arguments
args="scripts/kimimaro_boxes_new.txt $SLURM_ARRAY_TASK_ID"
echo "all arguments: "
echo $args

python scripts/skeleton_slurm.py $args

