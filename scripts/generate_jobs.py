
import numpy as np
import os
import sys
from itertools import product
import yaml
import shutil
import wkw
config = sys.argv[1]
#config = "running_configs/job_config.yml"
with open(config, 'r') as ymlfile:
    cfg = yaml.load(ymlfile,Loader=yaml.FullLoader)

coor_initial = np.array(cfg['jobs']['coor_initial'])
total_column_size = np.array(cfg['jobs']['total_column_size'])
output_shape = np.array(cfg['jobs']['output_shape'])
order = np.array(cfg['jobs']['order'])
base_dir = cfg['model']['base_dir']
folders = cfg['model']['folders']
res = cfg['model']['res']
ymlfile.close()
column_volume = np.prod(total_column_size)
time_per_voxel = 63/np.prod([624,624,142])
resolution = '/2-2-1/' if res == 2 else '/1/'

#create dataset directory
deletion = False
#for i in folders:
#    dir_ = base_dir+i+resolution
#    if not os.path.exists(dir_):
#        os.makedirs(dir_)
#    shutil.rmtree(dir_,ignore_errors=True)
#    wkw.Dataset.create(dir_,  wkw.Header(np.uint8))
#    deletion = True
print(deletion)
#
num_jobs = int(time_per_voxel*column_volume//3600) +1
volume_per_job = column_volume//num_jobs
# assert volume_per_job % prediction_cube_size == 0
job_box_dim = output_shape.copy()
if volume_per_job//(total_column_size[order[0]]*total_column_size[order[1]]*output_shape[order[2]]):
    #we can fill x and z completely
    job_box_dim[order[:2]] = total_column_size[order[:2]]
elif volume_per_job//(total_column_size[order[0]]*output_shape[order[1]]*output_shape[order[2]]):
    job_box_dim[order[0]] = total_column_size[order[0]]
    job_box_dim[order[1]] = volume_per_job//(job_box_dim[order[0]]*job_box_dim[order[2]])
else:
    job_box_dim[order[0]] = volume_per_job//(job_box_dim[order[1]]*job_box_dim[order[2]])
job_box_dim = job_box_dim - (job_box_dim%output_shape)
num_boxes_per_dim = total_column_size//job_box_dim
num_jobs = np.prod(num_boxes_per_dim)
print(num_jobs)
print(*job_box_dim)

indices = np.arange(num_jobs)
iterator=indices.copy()
relative_mapping = [[] for i in range(len(order))]
for i in order[:-1]:
    relative_mapping[i] = iterator%num_boxes_per_dim[i]
    iterator = iterator//num_boxes_per_dim[i]
#go bottom up in last dimension
relative_mapping[order[-1]] = num_boxes_per_dim[order[-1]]-1-iterator
coordinate_mapping = coor_initial+np.transpose(np.stack(relative_mapping)*job_box_dim[:,None])

sizes = np.ones(num_jobs)[:,None]*job_box_dim[None,:]    
boxes = np.concatenate((coordinate_mapping,sizes),axis=-1).astype(np.int64)
print(*(num_boxes_per_dim*job_box_dim))
np.savetxt(sys.argv[2]+".txt",boxes,fmt="%d")
