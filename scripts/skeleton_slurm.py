import sys

sys.path.append('/u/vkhare/codat/')
import wkw
# skeleton tools
import kimimaro
from cloudvolume import PrecomputedSkeleton
# other tools
import time
import re
import scipy.io as sio
import os
import sys
from tqdm import tqdm
from types import SimpleNamespace
import datetime
import numpy as np
import glob
import time
import multiprocessing
from wkskel import Skeleton
import pickle
wkwPath = "/tmpscratch/webknossos/Connectomics_Department/2018-11-13_scMS109_1to7199_v01_mag2_varun_myelin_column/segmentation/1/"
wkwColor = "/tmpscratch/webknossos/Connectomics_Department/2018-11-13_scMS109_1to7199_v01_mag2_varun_myelin_column/color/1/"
vox_size = [8,8,35]
multiprocessing.process.current_process()._config['tempdir'] =  '/tmpscratch/vkhare/multiprocess'
dset = wkw.Dataset.open(wkwPath)
dsetColor = wkw.Dataset.open(wkwColor)
vessel_boxes=np.array([
[78939, 58518, 3631, 5872, 80072, 1324],
[81326, 58526, 4956, 1872, 14072, 300],
[78934, 55973, 4149, 5572, 2544, 1124],
[79336, 53412, 4297, 5572, 2561, 1124],
[79256, 50717, 4343, 4972, 2695, 1124],
[79756, 47471, 4543, 4472, 3072, 1124],
[79756, 44194, 4643, 4472, 3277, 1124],
[79756, 40423, 4943, 4472, 3771, 1124],
[79756, 37363, 5143, 5472, 3060, 1124],
[81256, 33715, 5543, 5472, 3648, 924],
[80939, 138858, 4031, 4572, 7821, 1024],
[80939, 146679, 4431, 4572, 7872, 924],
[80939, 154568, 4731, 4572, 3809, 824],
[80939, 158377, 4931, 4572, 3740, 824],
[80939, 162117, 5131, 4172, 3808, 794],
[80939, 165925, 5431, 3172, 4872, 694]
])
# Skeletonize:
##############
# From example on https://github.com/seung-lab/kimimaro
# A possible configuration for long thin axons
# without any somata in the field of view.
teasar_params = {
  # TEASAR parameters
  'scale': 4, # invalidation ball scale factor
  'const': 500, # invalidation ball const factor (in physical units)
  'pdrf_scale': 100000, # pdrf scale factor
  'pdrf_exponent': 4, # pdrf exponent

  # Special Soma handling, not applicable to this case
  'soma_detection_threshold': 99999999, # in physical units, set high to shut off
  'soma_acceptance_threshold': 99999999, # in physical units, set high to shut off
  'soma_invalidation_scale': 0.5, # Special invalidation ball for somata
  'soma_invalidation_const': 0, # Special invalidation ball for somata
}
def get_small_bboxes(bbox, small_bbox_size, overlap=1):
    # > Note : the returned small_bboxes are of size small_bbox_size + overlap
#     assert(np.all(np.mod(bbox[3:], small_bbox_size) == 0))
    num_boxes = np.uint32(bbox[3:] // small_bbox_size)
    small_bboxes = [np.concatenate((
                        bbox[0:3] + np.array([i, j, k]) * small_bbox_size, 
                        small_bbox_size + overlap)) 
                    for i in range(num_boxes[0])
                    for j in range(num_boxes[1])
                    for k in range(num_boxes[2])]
    return small_bboxes
def read_cube(bbox):
    xyz_min = bbox[0:3]
    xyz_diff = bbox[3:]
    cubeData = np.squeeze(dset.read(xyz_min, xyz_diff))
    cubeColor = np.squeeze(dsetColor.read(xyz_min, xyz_diff))
    return cubeData,cubeColor

def process_cube(small_bbox, wkw_path=wkwPath,vessel_boxes=vessel_boxes, vox_size=vox_size, progress=False):
    # read cube
    cubeData,cubeColor = read_cube(small_bbox)
    def get_intersection(c1,c2):
        if c1[0] < c2[0]:
            return (c2[0],min(c1[1],c2[1])) if c1[1] > c2[0] else (-1,-1)
        else:
            return (c1[0],min(c1[1],c2[1])) if c2[1] > c1[0] else (-1,-1)

    for blood_vessel in vessel_boxes:
        c1 = list(zip(list(small_bbox[:3]),list(small_bbox[:3]+small_bbox[3:])))
        c2 = list(zip(list(blood_vessel[:3]),list(blood_vessel[:3]+blood_vessel[3:])))
        mask = [get_intersection(*coords) for coords in list(zip(c1,c2))]
        if (-1,-1) not in mask:
            final_mask = np.array(mask)-small_bbox[:3,None]
            cubeData[tuple(slice(i[0],i[1]) for i in final_mask)]=0
        
    cubeData[cubeData==1]=0
    # if cubeData only contains 0 we just return
    if not np.any(cubeData > 0):
        return {}
    if np.sum(cubeColor<8)/np.prod(cubeColor.shape) > 0.08:
        return {}

    # run kimimaro
    skeletons = kimimaro.skeletonize(
       cubeData, 
       teasar_params=teasar_params, 
       anisotropy=vox_size, # in nanometers
       dust_threshold=1000, # in voxels
       progress=progress,
       fix_branching=True,
       fix_borders=True, # default True
       parallel=0
    )

    # now we still need to account for the offset from the bbox
    # > note that vertices are stored in nm units
    for key in skeletons.keys():
        skeletons[key].vertices += small_bbox[0:3] * vox_size

    return skeletons
def postprocess(skels):
    skel = PrecomputedSkeleton.simple_merge(skels).consolidate()
    skel = kimimaro.postprocess(
      skel, 
      dust_threshold=1000, # physical units
      tick_threshold=3500 # physical units
    )
    skel = skel.downsample(20)
    return skel
boxes = np.loadtxt(sys.argv[1]).astype(np.int64)
index_begin = int(sys.argv[2])
index=index_begin
small_bbox_size =np.array([512,512,512])
box = boxes[index,:]

if os.path.exists("/u/vkhare/codat/.temp_new/skeleton_{}.pkl".format(index)):
    print("file already exists")
    sys.exit(0)
small_boxes = get_small_bboxes(box,small_bbox_size)
# skeleton = process_cube(box)
all_skels=[]
for i in small_boxes:
    skel = process_cube(i)
    if 2 in skel:
        all_skels.append(skel[2])
# mp_pool = multiprocessing.Pool()
# skels_per_box = mp_pool.map(process_cube, small_boxes)
# mp_pool.close()
# mp_pool.join()
# all_skels = [skel[2] for skel in skels_per_box if 2 in skel]
skeleton = {2:PrecomputedSkeleton.simple_merge(all_skels).consolidate()}
with open('/u/vkhare/codat/.temp_new/skeleton_{}.pkl'.format(index), 'wb') as f:
    pickle.dump(skeleton, f, pickle.HIGHEST_PROTOCOL)
    print("wrote file to ",'/u/vkhare/codat/.temp_new/skeleton_{}.pkl'.format(index))
