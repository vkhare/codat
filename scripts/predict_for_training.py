import sys

sys.path.append('/u/vkhare/codat/')
import tensorflow as tf
import numpy as np
import codat.nn.unet
import codat.nn.deep_labv3 as deepnet
import wkw
import tqdm
import glob
import codat.training.get_empty_gpu
from collections import defaultdict
import scipy.io as sio
import os
from skimage import transform
from evaluation_utils.architectures import Architectures
from evaluation_utils.evaluate import Evaluate,PredHolder

codat.training.get_empty_gpu.get_empty_gpu()
#parser = argparse.ArgumentParser(description='Add model dir and other configs')
#parser.add_argument('--model_dir',default='',type=str,
#                    help='tensor board log dir (default: /tmpscratch/models/unet)')
#parser.add_argument('--output_dir',default='/tmpscratch/vkhare/models/unet',type=str,
#                    help='tensor board log dir (default: /tmpscratch/models/unet)')


data_format = 'channels_last'

DATA = "/tmpscratch/georgwie/artifacts/default/merge_mappings/mhlab_ds2__unet_d3_f16_b4_volume_50__column_06_24__segmentation__globalized_64__rag__agglomeration__merged_mappings/wkw_with_merged_mappings/" 
#'../myelin_seg_data/seg_dst_mask' #preloaded dst mask for speed
nodes = sio.loadmat('../myelin_seg_data/train.mat')
cubes_in = nodes['final'].T
num_Classes=3

folders = ['segmentation']
model_prefix = '/tmpscratch/vkhare/models/'
model_suffix = '/model.ckpt-'
model_name = sys.argv[1] 
step = str(sys.argv[2])
bbox = [105776, 165179, 4731, 1000, 1000, 230]
#bbox = [int(sys.argv[5]),int(sys.argv[6]),int(sys.argv[7]),
#        int(sys.argv[8]),int(sys.argv[9]),int(sys.argv[10])]
thresh_myelin = float(sys.argv[5])
thresh_inside = float(sys.argv[6])
ckpt = model_prefix+model_name+model_suffix+step


base_dir = '/tmpscratch/webknossos/Connectomics_Department/2018-11-13_scMS109_1to7199_v01_mag2_varun_myelin_training_predictions/'
architecture = Architectures(type=int(sys.argv[3]), res=int(sys.argv[4]),
                             ckpt=ckpt, DATA = DATA, predict_size=bbox[3:6])
pred_soft = architecture.predict(bbox[:3],preload=True,wkw_out=base_dir,
                                 thresh_inside=thresh_inside,
                                 thresh_myelin=thresh_myelin,
                                 folders=['segmentation'])




