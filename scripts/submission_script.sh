#!/bin/bash
exp_basename="myelin_column"
cd /u/vkhare/codat/
pwd
num_cores=32 # or 32
echo "evaluating number of jobs to run"
mapfile -t output < <(python scripts/generate_jobs.py $1 $2)
echo "delete exisiting target directory: ${output[0]}"
echo "number of jobs are: ${output[1]}"
echo "dimensions of each job: ${output[2]}"
echo "dimensions of total box predicted: ${output[3]}"
echo "index file written at ${2}.txt"
num_jobs=${output[1]} 
sbatch --cpus-per-task=$3\
       --time=100:00:00 \
       --job-name="cpu_pred_myelin"\
       --gres=gpu:$4 \
       --mem=128G \
       --nice=2073741822 \
       --array=0-$num_jobs \
       --export=exp_basename=$exp_basename,config=$1,index_file=${2} \
       -o /tmpscratch/vkhare/slurm-%A_%a.out scripts/single_run.sh
echo ""
#0-$num_jobs \
