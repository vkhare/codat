# data tools
import numpy as np
import wkw
# skeleton tools
import kimimaro
from cloudvolume import PrecomputedSkeleton
# other tools
import time
import re
import scipy.io as sio
import os
import sys
from tqdm import tqdm
from types import SimpleNamespace
from skimage.transform import rescale, resize, downscale_local_mean
import datetime
import numpy as np
import glob
import matplotlib.pyplot as plt
from scipy.ndimage.measurements import label,find_objects
from scipy import ndimage
from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = "all"
from scipy.ndimage.morphology import binary_dilation, generate_binary_structure, iterate_structure
# from scipy.ndimage.morphology import binary_closing, binary_dilation, binary_erosion
# from scipy.ndimage.measurements import label
import time
import multiprocessing
# import cfut
# from concurrent.futures import as_completed
import subprocess
from wkskel import Skeleton
vessel_boxes=np.array([
[78939, 58518, 3631, 5872, 80072, 1324],
[78934, 55973, 4149, 5572, 2544, 1124],
[79336, 53412, 4297, 5572, 2561, 1124],
[79256, 50717, 4343, 4972, 2695, 1124],
[79756, 47471, 4543, 4472, 3072, 1124],
[79756, 44194, 4643, 4472, 3277, 1124],
[79756, 40423, 4943, 4472, 3771, 1124],
[79756, 37363, 5143, 5472, 3060, 1124],
[81256, 33715, 5543, 5472, 3648, 924],
[80939, 138858, 4031, 4572, 7821, 1024],
[80939, 146679, 4431, 4572, 7872, 924],
[80939, 154568, 4731, 4572, 3809, 824],
[80939, 158377, 4931, 4572, 3740, 824],
[80939, 162117, 5131, 4172, 3808, 794],
[80939, 165925, 5431, 3172, 4872, 694]
])
wkwPath = "/tmpscratch/webknossos/Connectomics_Department/2018-11-13_scMS109_1to7199_v01_mag2_varun_myelin_column/segmentation/1/"
vox_size = [8,8,35]
num_cores= 32
dset = wkw.Dataset.open(wkwPath)
def read_cube(bbox):
    xyz_min = bbox[0:3]
    xyz_diff = bbox[3:]
    cubeData = np.squeeze(dset.read(xyz_min, xyz_diff))
    return cubeData
def get_small_bboxes(bbox, small_bbox_size, overlap=1):
    # > Note : the returned small_bboxes are of size small_bbox_size + overlap
#     assert(np.all(np.mod(bbox[3:], small_bbox_size) == 0))
    num_boxes = np.uint32(bbox[3:] // small_bbox_size)
    small_bboxes = [np.concatenate((
                        bbox[0:3] + np.array([i, j, k]) * small_bbox_size, 
                        small_bbox_size + overlap)) 
                    for i in range(num_boxes[0])
                    for j in range(num_boxes[1])
                    for k in range(num_boxes[2])]
    return small_bboxes

def process_cube(small_bbox, wkw_path=wkwPath,vessel_boxes=vessel_boxes, vox_size=vox_size, progress=False):
    # read cube
    cubeData = read_cube(small_bbox)
    def get_intersection(c1,c2):
        if c1[0] < c2[0]:
            return (c2[0],min(c1[1],c2[1])) if c1[1] > c2[0] else (-1,-1)
        else:
            return (c1[0],min(c1[1],c2[1])) if c2[1] > c1[0] else (-1,-1)

    for blood_vessel in vessel_boxes:
        c1 = list(zip(list(small_bbox[:3]),list(small_bbox[:3]+small_bbox[3:])))
        c2 = list(zip(list(blood_vessel[:3]),list(blood_vessel[:3]+blood_vessel[3:])))
        mask = [get_intersection(*coords) for coords in list(zip(c1,c2))]
        if (-1,-1) not in mask:
            final_mask = np.array(mask)-small_bbox[:3,None]
            cubeData[tuple(slice(i[0],i[1]) for i in final_mask)]=0
        
    cubeData[cubeData==1]=0
    # if cubeData only contains 0 we just return
    if not np.any(cubeData > 0):
        return
    return

small_boxes = get_small_bboxes(np.array([81071,68905,3109,3072,3072,1024]),np.array([512,512,512]))
for box in small_boxes:
    process_cube(box)
# mp_pool = multiprocessing.Pool()
# skels_per_box = mp_pool.map(process_cube,small_boxes)
# mp_pool.close()
# mp_pool.join()