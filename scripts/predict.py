import sys

sys.path.append('/u/vkhare/codat/')
import tensorflow as tf
import numpy as np
import codat.nn.unet
import codat.nn.deep_labv3 as deepnet
import wkw
import tqdm
import glob
import codat.training.get_empty_gpu
from collections import defaultdict
from scipy import io as sio
import os
from skimage import transform
from evaluation_utils.architectures import Architectures
from evaluation_utils.evaluate import Evaluate,PredHolder
import yaml

config = sys.argv[1]

#config = "running_configs/job_config.yml"
with open(config, 'r') as ymlfile:
    cfg = yaml.load(ymlfile,Loader=yaml.FullLoader)
ymlfile.close()
boxes = np.loadtxt(sys.argv[2]+".txt")
index = int(sys.argv[3])
if len(boxes.shape) == 1 and index == 0:
    bbox = boxes[:3]
    size = boxes[3:]
elif len(boxes.shape) > 1:
    bbox = boxes[index,:3]
    size = boxes[index,3:].astype(np.int64)
else:
    print("index out of bounds")
    sys.exit(1)
print(bbox,size)
if cfg['model']['use_gpu']:
    codat.training.get_empty_gpu.get_empty_gpu()
else:
    os.environ["CUDA_VISIBLE_DEVICES"]=""

data_format = cfg['model']['data_format'] 
DATA = cfg['model']['data'] 
#'../myelin_seg_data/seg_dst_mask' #preloaded dst mask for speed
nodes = sio.loadmat(cfg['model']['nodes'])
cubes_in = nodes['final'].T
num_Classes=cfg['model']['num_classes']

folders = cfg['model']['folders']
model_prefix = cfg['model']['model_prefix']
model_suffix = cfg['model']['model_suffix']
model_name = cfg['model']['model_name'] 
step = str(cfg['model']['train_step'])
thresh_myelin = cfg['model']['thresh_myelin']
thresh_inside = cfg['model']['thresh_inside']
ckpt = model_prefix+model_name+model_suffix+step

base_dir = cfg['model']['base_dir']
architecture = Architectures(type=cfg['model']['type'], res=cfg['model']['res'],
                             ckpt=ckpt, DATA = DATA, predict_size=size)
pred_soft = architecture.predict(bbox,preload=False,wkw_out=base_dir,
                                 thresh_inside=thresh_inside,thresh_myelin=thresh_myelin,
                                 folders=folders)




