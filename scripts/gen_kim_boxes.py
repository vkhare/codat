import numpy as np
import sys
def get_small_bboxes(bbox, small_bbox_size, overlap=1):
    # > Note : the returned small_bboxes are of size small_bbox_size + overlap
#     assert(np.all(np.mod(bbox[3:], small_bbox_size) == 0))
    num_boxes = np.uint32(bbox[3:] // small_bbox_size)
    small_bboxes = [np.concatenate((
                        bbox[0:3] + np.array([i, j, k]) * small_bbox_size, 
                        small_bbox_size + overlap)) 
                    for i in range(num_boxes[0])
                    for j in range(num_boxes[1])
                    for k in range(num_boxes[2])]
    return small_bboxes
bigBox = np.array([78000,35124,40,44160,144348,7068])
small_bbox_size =np.array([1536*2-1,1536*2-1,512*2-1])

small_boxes = np.array(get_small_bboxes(bigBox,small_bbox_size)).astype(np.int64)
np.savetxt(sys.argv[1]+".txt",small_boxes,fmt="%d")