#!/bin/bash
source ~/.bashrc
#### Standard values of Variables
# FLAGS
SLURM_ARRAY_TASK_ID=${SLURM_ARRAY_TASK_ID:-}
JOB_ID=${JOB_ID:-${SLURM_ARRAY_JOB_ID}}
JOB_NAME=${JOB_NAME:-${SLURM_JOB_NAME}}
exp_basename=${exp_basename:-"testset_run_27.01.2020"}

# Directories
py_script_path=${py_script_path:-"/u/vkhare/codat"}

LD_LIBRARY_PATH=/u/vkhare/conda-envs/tfCPU/lib/:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH
echo "the ld library path is $LD_LIBRARY_PATH"

if (([ $(hostname) != "gaba" ] && [ $(hostname) != "gaba01" ]) && [ $(hostname) != "gaba02" ])
then
	export SGE_CELL="GABA"
	export MODULEPATH=${MODULEPATH}:/afs/ipp/common/usr/modules.2017/@sys/modulefiles/${SGE_CELL}/applications:/afs/ipp/common/usr/modules.2017/@sys/modulefiles/${SGE_CELL}/libs:/afs/ipp/common/usr/modules.2017/@sys/modulefiles/${SGE_CELL}/tools:/afs/ipp/common/usr/modules.2017/@sys/modulefiles/${SGE_CELL}/GPU
	echo 'Modules are available now'
fi


# cd to script path
cd $py_script_path
module load anaconda/3/5.1.0
#module load cuda/9.0
#module load cudnn/7.4-cuda-9.0
module load tensorflow/1.12-cpu

conda activate tfCPU

# Set arguments
args="$config $index_file $SLURM_ARRAY_TASK_ID"
echo "all arguments: "
echo $args

# Set the number of cores available per process if the $SLURM_CPUS_PER_TASK is set
if [ ! -z $SLURM_CPUS_PER_TASK ] ; then
    export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
else
    export OMP_NUM_THREADS=1
fi

python scripts/predict.py $args

