from setuptools import setup

setup(name='codat',
      version='0.1',
      description='Connectomic data analysis toolkit',
      url='https://gitlab.mpcdf.mpg.de/connectomics/codat',
      author='Benedikt Staffler',
      author_email='benedikt.staffler@brain.mpg.de',
      packages=['codat']
      )